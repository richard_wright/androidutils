package net.wrightnz.android.three.geom;

import android.graphics.Point;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static net.wrightnz.android.three.geom.GeomUtils.groupContainsVertices;
import static net.wrightnz.android.three.geom.GeomUtils.isCloseEnough;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Richard Wright (Spark NZ Ventures) on 1/06/17.
 */

public class GeomUtilsTest {

    @Test
    public void isCloseEnoughTest() {
        System.out.println("Testing: isCloseEnough()");
        Point p1 = new Point(5, 12);
        Point p2 = new Point(3, 15);
        assertTrue(isCloseEnough(p1, p2));
    }

    @Test
    public void groupContainsVerticesTest(){
        Vertex v1 = new Vertex(0.5F, 0.0F, 0.5F, 1);
        Vertex v2 = new Vertex(0.0F, 0.0F, 0.5F, 2);
        Vertex v3 = new Vertex(0.5F, 0.0F, 0.0F, 3);
        Vertex v4 = new Vertex(0.0F, 0.0F, 0.0F, 4);
        Vertex v5 = new Vertex(0.0F, 0.5F, 0.0F, 5);

        List<Vertex> base = new ArrayList<>();
        base.add(v1);
        base.add(v2);
        base.add(v3);
        base.add(v4);
        Face face0 = new Face();
        face0.setVertices(base);

        List<Vertex> side = new ArrayList<>();
        side.add(v1);
        side.add(v2);
        side.add(v5);
        Face face1 = new Face();
        face1.setVertices(side);

        Group group = new Group();
        group.addFace(face0);
        group.addFace(face1);

        List<Vertex> vertices2 = new ArrayList<>();
        vertices2.add(v1);
        vertices2.add(v2);
        assertTrue(groupContainsVertices(group,  vertices2));

        List<Vertex> vertices3 = new ArrayList<>();
        vertices3.add(v3);
        vertices3.add(v5);
        assertFalse(groupContainsVertices(group,  vertices3));
    }
}
