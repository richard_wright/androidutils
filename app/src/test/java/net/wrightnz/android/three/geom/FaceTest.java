package net.wrightnz.android.three.geom;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Richard Wright (Spark NZ Ventures) on 27/06/17.
 *
 * @author Richard Wright
 */
public class FaceTest {

    @Test
    public void testFacesEqual() {
        List<Vertex> vertices1 = new ArrayList<>();
        vertices1.add(new Vertex(1.0F, 1.0F, 1.0F, 1));
        vertices1.add(new Vertex(0.0F, 0.0F, 0.0F, 2));
        vertices1.add(new Vertex(0.1F, 0.0F, 0.0F, 3));
        Face face1 = new Face();
        face1.setVertices(vertices1);

        List<Vertex> vertices2 = new ArrayList<>();
        vertices2.add(new Vertex(1.0F, 1.0F, 1.0F, 1));
        vertices2.add(new Vertex(0.0F, 0.0F, 0.0F, 3));
        vertices2.add(new Vertex(0.1F, 0.0F, 0.0F, 2));
        Face face2 = new Face();
        face2.setVertices(vertices2);
        Assert.assertTrue(face1.equals(face2));
    }

}
