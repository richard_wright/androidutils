package net.wrightnz.android.three;

import net.wrightnz.android.three.geom.Vertex;

import java.util.Comparator;

/**
 * Created by richa on 2/08/2017.
 */

public class VertexComparator implements Comparator<Vertex> {

    @Override
    public int compare(Vertex vertex, Vertex t1) {
        if (vertex.getX() > t1.getX()) {
            return 1;
        } else if (vertex.getX() < t1.getX()) {
            return -1;
        } else {
            return 0;
        }
    }
}

