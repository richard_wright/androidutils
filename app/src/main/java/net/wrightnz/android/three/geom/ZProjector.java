package net.wrightnz.android.three.geom;

import java.util.List;

/**
 * Created by Richard Wright on 13/10/17.
 *
 * @author Richard Wright.
 */
public class ZProjector {

    private int currentVertexIndex;

    public ZProjector(int currentVertexIndex) {
        this.currentVertexIndex = currentVertexIndex;
    }

    public Face projectZ(List<Vertex> vertices, List<Vertex> orderedVertices, float amount) {
        Face face = new Face();
        Vertex firstVertex = null;
        for (Vertex vertex : orderedVertices) {
            Vertex v = new Vertex(vertex.getX(), vertex.getY(), amount, currentVertexIndex++);
            if (firstVertex == null) {
                firstVertex = v;
            }
            vertices.add(v);
            face.addVertex(v);
        }
        if (firstVertex != null) {
            face.addVertex(firstVertex);
        }
        return face;
    }

    public int getCurrentVertexIndex() {
        return currentVertexIndex;
    }

}
