package net.wrightnz.android.three.awt;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created for (Spark NZ) on 13/12/17.
 *
 * @author Richard Wright
 */

public class Graphics {

    private Canvas canvas;
    private Paint paint;

    public void fillRect(int top, int left, int width, int height) {

    }

    public void setColor(int color) {
        paint.setColor(color);
    }

    public void drawRect(int x, int y, int width, int height) {
    }


    public Canvas getCanvas() {
        return canvas;
    }

    public void setCanvas(Canvas canvas) {
        this.canvas = canvas;
    }

    public Paint getPaint() {
        if (paint == null) {
            paint = new Paint();
            paint.setColor(Color.GREEN);
            paint.setAntiAlias(true);
            paint.setStrokeWidth(6);
        }
        return paint;
    }

    public void setPaint(Paint paint) {
        this.paint = paint;
    }
}
