/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.wrightnz.android.three.geom;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Richard Wright
 */
public class Face implements Parcelable {

    private List<Vertex> vertices = new ArrayList<>();
    private boolean selected;

    public Face(){
    }

    public Face(List<Vertex> vertices){
        this.vertices = vertices;
    }

    protected Face(Parcel in) {
        vertices = in.createTypedArrayList(Vertex.CREATOR);
        selected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(vertices);
        dest.writeByte((byte) (selected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Face> CREATOR = new Creator<Face>() {
        @Override
        public Face createFromParcel(Parcel in) {
            return new Face(in);
        }

        @Override
        public Face[] newArray(int size) {
            return new Face[size];
        }
    };

    /**
     * @return the vertices
     */
    public List<Vertex> getVertices() {
        return vertices;
    }

    /**
     * @param vertices the vertices to set
     */
    public void setVertices(List<Vertex> vertices) {
        this.vertices = vertices;
    }

    public void addVertex(Vertex vertex) {
        this.vertices.add(vertex);
    }

    public void removeVertex(Vertex vertex) {
        this.vertices.remove(vertex);
    }

    /**
     * @return the selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("f ");
        for(Vertex v : vertices){
            sb.append(v.getIndex());
            sb.append(" ");
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj){
        if(obj == null){
            return false;
        }
        if(!(obj instanceof Face)){
            return false;
        }
        Face that = (Face)obj;
        if(hasSameVertices(that)){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + (this.vertices != null ? this.vertices.hashCode() : 0);
        return hash;
    }

    private boolean hasSameVertices(Face that){
        List<Vertex> theseVerts = this.getVertices();
        List<Vertex> thoseVerts = that.getVertices();
        if(theseVerts.size() != thoseVerts.size()){
            return false;
        }
        for(Vertex vertex : theseVerts){
            if(!thoseVerts.contains(vertex)){
                return false;
            }
        }
        return true;
    }

}
