package net.wrightnz.android.three.database;

import net.wrightnz.android.easydb.DatabaseDefinition;
import net.wrightnz.android.easydb.Table;

import java.util.Collections;
import java.util.List;

/**
 * Created on 20/12/17.
 *
 * @author Richard Wright
 */

public class CacheDefinition implements DatabaseDefinition {

    private List<CacheTable> tables = Collections.singletonList(new CacheTable());

    /**
     *
     * @return the name of the database/SQLite database file.
     */
    @Override
    public String getName() {
        return "threed.db";
    }

    /**
     * If you change the database schema, you must increment the database version.
     * @return the current database version.
     */
    @Override
    public int getVersion() {
        return 1;
    }

    @Override
    public List<? extends Table> getTables() {
        return tables;
    }

}
