package net.wrightnz.android.three.service;

import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

import net.wrightnz.android.three.Object3DGenerator;
import net.wrightnz.android.three.geom.Object3d;
import net.wrightnz.android.utils.BroadcastUtils;
import net.wrightnz.android.utils.ImageUtils;
import net.wrightnz.android.utils.LogUtils;
import net.wrightnz.android.utils.net.ServiceResponseCodes;

import java.io.IOException;

/**
 * Created on 6/12/17.
 *
 * @author Richard Wright
 */
public class ImageInterpreterIntentService extends IntentService {

    private static final String TAG = ImageInterpreterIntentService.class.getSimpleName();
    public static final String ACTION_FIND_EDGES = "ACTION_FIND_EDGES";

    public ImageInterpreterIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (ACTION_FIND_EDGES.equals(intent.getAction())) {
            findEdges(intent);
        }
    }

    private void findEdges(Intent intent) {
        /*try {
            BroadcastUtils.sendBroadcast(this, intent.getAction(), ServiceResponseCodes.OK, edgesUri);
        } catch (Exception e) {
            BroadcastUtils.sendBroadcast(this, intent.getAction(), ServiceResponseCodes.FAILED, null);
        }*/
    }

}
