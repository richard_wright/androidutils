package net.wrightnz.android.three;

/**
 * Created by Richard Wright (Spark NZ Ventures) on 29/05/17.
 */

public final class ResourceIds {

    public static final int[] RESOURCE_IDS = {R.string.api_protocol,
            R.bool.is_production,
            R.string.user_agent,
            R.integer.connection_timeout_mills,
            R.integer.read_timeout_mills,
            R.string.api_domain};
}
