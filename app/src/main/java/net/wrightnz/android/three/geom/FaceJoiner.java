package net.wrightnz.android.three.geom;

/**
 * Created by Richard Wright on 13/10/17.
 *
 * @author Richard Wright.
 */
public class FaceJoiner {

    public FaceJoiner() {
    }

    public Face join(Face face1, Face face2) {
        Face face = new Face();
        for (int i = 0; i < face1.getVertices().size(); i++) {
            face.addVertex(face1.getVertices().get(i));
            face.addVertex(face2.getVertices().get(i));
        }
        return face;
    }

}
