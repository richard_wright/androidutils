/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.wrightnz.android.three.geom;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Richard Wright richard@wrightnz.net
 */
public class Group extends GroupOfFaces implements Parcelable {

    /**
     * The MaterialGroups in this Group.
     */
    private List<MaterialGroup> materialGroups = new ArrayList<>();

    public Group(){

    }

    public Group(String name){
        super(name);
    }

    protected Group(Parcel in) {
        super(in);
        materialGroups = in.createTypedArrayList(MaterialGroup.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(materialGroups);
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return super.getName();
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        super.setName(groupName);
    }


    /**
     * @return the materialGroups
     */
    public List<MaterialGroup> getMaterialGroups() {
        return materialGroups;
    }

    /**
     * @param materialGroups the materialGroups to set
     */
    public void setMaterialGroups(List<MaterialGroup> materialGroups) {
        this.materialGroups = materialGroups;
    }

    /**
     *
     * @param materialGroup to add to this group.
     */
    public void addMaterialGroup(MaterialGroup materialGroup) {
        this.materialGroups.add(materialGroup);
    }

    @NonNull
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("g ");
        sb.append(this.getGroupName());
        sb.append("\n");
        for (Face face : super.getFaces()) {
            sb.append(face.toString());
            sb.append("\n");
        }
        for (MaterialGroup mg : getMaterialGroups()) {
            sb.append(mg.toString());
            sb.append("\n");
        }
        return sb.toString();
    }


    public static final Creator<Group> CREATOR = new Creator<Group>() {
        @Override
        public Group createFromParcel(Parcel in) {
            return new Group(in);
        }

        @Override
        public Group[] newArray(int size) {
            return new Group[size];
        }
    };

}
