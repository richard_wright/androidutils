/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.wrightnz.android.three.geom;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Richard Wright
 */
public class GroupOfFaces implements Parcelable {

    /**
     * The name of this Group of faces
     */
    private String name;

    /**
     * The faces in this GroupOfFaces.
     */
    private List<Face> faces = new ArrayList<Face>();

    public GroupOfFaces() {

    }

    public GroupOfFaces(String name) {
        this.name = name;
    }

    protected GroupOfFaces(Parcel in) {
        name = in.readString();
        faces = in.createTypedArrayList(Face.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeTypedList(faces);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<GroupOfFaces> CREATOR = new Creator<GroupOfFaces>() {
        @Override
        public GroupOfFaces createFromParcel(Parcel in) {
            return new GroupOfFaces(in);
        }

        @Override
        public GroupOfFaces[] newArray(int size) {
            return new GroupOfFaces[size];
        }
    };

    /**
     * @return the name of
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the faces
     */
    public List<Face> getFaces() {
        return faces;
    }

    /**
     * @param faces the faces to set
     */
    public void setFaces(List<Face> faces) {
        this.faces = faces;
    }

    public void addFace(Face face){
        this.faces.add(face);
    }

}
