package net.wrightnz.android.three.geom;

import java.util.List;

/**
 * BoundingBox.java
 * <p>
 * A Bounding box object that defines a box which completely includes
 * the displayed List of vertices.
 */
public class BoundingBox {

    private Point3D min = new Vertex(0, 0, 0);
    private Point3D max = new Vertex(0, 0, 0);

    public BoundingBox(List<Vertex> vertices) {
        if (!vertices.isEmpty()) {
            Vertex firstVertex = vertices.get(0);
            float xmin = firstVertex.getX(), xmax = xmin;
            float ymin = firstVertex.getY(), ymax = ymin;
            float zmin = firstVertex.getZ(), zmax = zmin;

            for (Vertex v : vertices) {
                float x = v.getX();
                if (x < xmin) {
                    xmin = x;
                }
                if (x > xmax) {
                    xmax = x;
                }
                float y = v.getY();
                if (y < ymin) {
                    ymin = y;
                }
                if (y > ymax) {
                    ymax = y;
                }
                float z = v.getZ();
                if (z < zmin) {
                    zmin = z;
                }
                if (z > zmax) {
                    zmax = z;
                }
            }
            min = new Vertex(xmin, ymin, zmin);
            max = new Vertex(xmax, ymax, zmax);
        }
    }

    public void combine(BoundingBox bb) {
        Point3D p = bb.min;
        if (p.getX() < min.getX()) {
            min.setX(p.getX());
        }
        if (p.getY() < min.getY()) {
            min.setY(p.getY());
        }
        if (p.getZ() < min.getZ()) {
            min.setZ(p.getZ());
        }

        p = bb.max;
        if (p.getX() > max.getX()) {
            max.setX(p.getX());
        }
        if (p.getY() > max.getY()) {
            max.setY(p.getY());
        }
        if (p.getZ()  > max.getZ()) {
            max.setZ(p.getZ());
        }
    }

    public Point3D getCenter() {
        float xmid = (min.getX() + max.getX()) / 2;
        float ymid = (min.getY() + max.getY()) / 2;
        float zmid = (min.getZ() + max.getZ()) / 2;
        return new Point3D(xmid, ymid, zmid);
    }

    public float getWidth() {
        return max.getY() - min.getX();
    }

    public float getHeight() {
        return max.getY() - min.getY();
    }

    public float getDepth() {
        return max.getZ() - min.getZ();
    }

    public float getMinX(){
        return min.getX();
    }

    public float getMaxX(){
        return max.getX();
    }

    public float getMinY(){
        return min.getY();
    }

    public float getMaxY(){
        return max.getY();
    }

    public float getMinZ(){
        return min.getZ();
    }

    public float getMaxZ(){
        return max.getZ();
    }

}
