/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.wrightnz.android.three.geom;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author richard wright
 */
public class MaterialGroup extends GroupOfFaces implements Parcelable {

    public MaterialGroup() {

    }

    protected MaterialGroup(Parcel in) {
        super(in);
    }

    /**
     * @return the materialName
     */
    public String getMaterialName() {
        return super.getName();
    }

    /**
     * @param materialName the materialName to set
     */
    public void setMaterialName(String materialName) {
        super.setName(materialName);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("usemtl ");
        sb.append(this.getMaterialName());
        sb.append("\n");
        for (Face f : super.getFaces()) {
            sb.append(f);
            sb.append("\n");
        }
        return sb.toString();
    }


    public static final Creator<MaterialGroup> CREATOR = new Creator<MaterialGroup>() {
        @Override
        public MaterialGroup createFromParcel(Parcel in) {
            return new MaterialGroup(in);
        }

        @Override
        public MaterialGroup[] newArray(int size) {
            return new MaterialGroup[size];
        }
    };

}
