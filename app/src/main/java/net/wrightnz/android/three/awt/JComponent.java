package net.wrightnz.android.three.awt;

import android.util.Size;

/**
 * Created for (Spark NZ) on 13/12/17.
 *
 * @author Richard Wright
 */

public class JComponent {

    private Size size;

    protected Size getMinimumSize() {
        return new Size(100, 100);
    }

    protected void setSize(Size size) {
        this.size = size;
    }

    protected Size getSize() {
        return this.size;
    }

    protected void repaint() {
    }

    public void paint(Graphics g) {

    }

    protected void addMouseListener(MouseListener listener) {

    }

    protected void addMouseMotionListener(MouseMotionListener listener) {

    }

}
