package net.wrightnz.android.three;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class IntentHandlerActivity extends AppCompatActivity {

    private static final String TAG = IntentHandlerActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent_handler);
        Intent intent = getIntent();
        Log.d(TAG, "Intent is: " + intent);
        if (intent != null) {
            /*
            Log.d(TAG, "Intent data is: " + intent.getData());
            try {
                importData(intent.getData());
            } catch (IOException e) {
                Log.wtf(TAG, "", e);
            }*/
            Intent main = new Intent(this, MainActivity.class);
            main.setData(intent.getData());
            startActivity(main);
            finish();
        }
    }

    private void importData(Uri data) throws IOException {
        ContentResolver cr = getContentResolver();
        final String scheme = data.getScheme();
        Log.d(TAG, scheme);
        if (ContentResolver.SCHEME_CONTENT.equals(scheme)) {
            InputStream is = cr.openInputStream(data);
            if (is == null) {
                return;
            }
            try {
                StringBuilder buf = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                String str;
                while ((str = reader.readLine()) != null) {
                    buf.append(str);
                    buf.append("\n");
                }
                Log.d(TAG, buf.toString());
            } finally {
                is.close();
            }
        }
    }

}
