/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.wrightnz.android.three;


import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;

import net.wrightnz.android.three.awt.Graphics;
import net.wrightnz.android.three.awt.JComponent;
import net.wrightnz.android.three.awt.MouseListener;
import net.wrightnz.android.three.awt.MouseMotionListener;
import net.wrightnz.android.three.geom.BoundingBox;
import net.wrightnz.android.three.geom.Face;
import net.wrightnz.android.three.geom.GeomUtils;
import net.wrightnz.android.three.geom.Matrix3D;
import net.wrightnz.android.three.geom.Object3d;
import net.wrightnz.android.three.geom.Point3D;
import net.wrightnz.android.three.geom.actions.Action;
import net.wrightnz.android.three.geom.actions.DeleteFaceAction;
import net.wrightnz.android.three.geom.actions.FaceActioner;

/**
 * An JComponent to display 3D models in Light Wave .obj format.
 *
 *  @author Richard Wright, richard@wrightnz.net
 */
public class Component3D extends JComponent implements MouseListener, MouseMotionListener{

    private static final int DEFAULT_WIDTH = 400;
    private static final int DEFAULT_HEIGHT = 400;

    private Object3d object;
    private float xfac;
    private int prevScaleIndicator = 0;
    private int prevx;
    private int prevy;
    private static final float SCALE_FUDGE = 1f;

    private Matrix3D matrix = new Matrix3D();
    private Matrix3D transformMatrix = new Matrix3D();
    
    private Rect selection = new Rect();

    // Box which contains the displayed objects list of vertices
    private BoundingBox boundingBox;
    private boolean isMoveMode = false;

    private View view;

    public Component3D(View view, Object3d obj) {
        this(view);
        this.addObject(obj);
    }

    public Component3D(View view){
        this.view = view;
        this.setSize(this.getMinimumSize());
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
    }

    private void addObject(Object3d object) {
        this.object = object;
        boundingBox = new BoundingBox(object.getVertices());
        float xw = boundingBox.getWidth();
        float yw = boundingBox.getHeight();
        float zw = boundingBox.getDepth();
        if (yw > xw) {
            xw = yw;
        }
        if (zw > xw) {
            xw = zw;
        }
        float f1 = getSize().getWidth() / xw;
        float f2 = getSize().getHeight() / xw;
        xfac = 0.7f * (f1 < f2 ? f1 : f2) * SCALE_FUDGE;
        repaint();
    }

    /**
     * @return the Object3D displayed by this Component3D.
     */
    public Object3d getObject() {
        return object;
    }

    public void clearObject() {
        this.object = null;
        repaint();
    }

    /**
     * Update the location of the selected vertices by the amounts carried in
     * the input point. e.g. if point was 0,1,1 this would move all the selected
     * vertice nothing in X, 1 in Y and 1 in Z.
     * 
     * @param move
     */
    public void moveSelectedVertice(Point3D move) {
        object.moveSelectedVertices(move);
        boundingBox = new BoundingBox(object.getVertices());
        repaint();
    }

    public void deleteSelectedVertice(){
        object.deleteSelectedVertices();
        boundingBox = new BoundingBox(object.getVertices());
        repaint();
    }

    public void deleteSelectedFace(Paint paint) {
        Face face = GeomUtils.makeFaceFromVertices(object.getSelectedVertices());
        Action deleteFaceAction = new DeleteFaceAction(face);
        FaceActioner.performAction(object, deleteFaceAction, paint);
        boundingBox = new BoundingBox(object.getVertices());
        repaint();
    }

    public void wieldSelected() {
        object.wieldSelected();
        repaint();
    }

    public void addPloygon() {
        object.addPolygon();
        boundingBox = new BoundingBox(object.getVertices());
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, getSize().getWidth(), getSize().getHeight());
        g.setColor(Color.RED);
        g.drawRect(selection.left, selection.top, selection.height(), selection.width());
        matrix.unit();
        Point3D center = boundingBox.getCenter();
        // Move origin to center of object.
        matrix.translate(-center.getX(), -center.getY(), -center.getZ());
        // Apply and transforms to the matrix
        matrix.mult(transformMatrix);
        // Scale
        matrix.scale(xfac, -xfac, 16 * xfac / getSize().getWidth());
        // Move origin to center of screen.
        matrix.translate(getSize().getWidth() / 2, getSize().getHeight() / 2, 8);
        // Transform all verts using the resulting matrix
        object.transform(matrix);
        object.paint(g.getCanvas(), g.getPaint());
    }


    public void viewFront() {
        object.resetTransformedVertices();
        transformMatrix = new Matrix3D();
        repaint();
    }

    public void viewSide() {
        object.resetTransformedVertices();
        transformMatrix = new Matrix3D();
        transformMatrix.yRotate(90);
        repaint();
    }

    public void viewTop() {
        object.resetTransformedVertices();
        transformMatrix = new Matrix3D();
        transformMatrix.xRotate(90);
        repaint();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean retVal1 = false; // gestureDetector.onTouchEvent(event);
        boolean retVal2 = false; // scaleGestureDetector.onTouchEvent(event); */
        return retVal1 || retVal2 || view.onTouchEvent(event);
    }
}
