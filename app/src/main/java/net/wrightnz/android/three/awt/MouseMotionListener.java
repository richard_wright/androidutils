package net.wrightnz.android.three.awt;

import android.view.MotionEvent;

/**
 * Created for (Spark NZ) on 13/12/17.
 *
 * @author Richard Wright
 */

public interface MouseMotionListener {

    boolean onTouchEvent(MotionEvent event);
}
