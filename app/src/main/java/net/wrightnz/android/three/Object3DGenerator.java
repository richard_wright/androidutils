package net.wrightnz.android.three;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;

import net.wrightnz.android.three.geom.BoundingBox;
import net.wrightnz.android.three.geom.Face;
import net.wrightnz.android.three.geom.FaceJoiner;
import net.wrightnz.android.three.geom.Group;
import net.wrightnz.android.three.geom.Object3d;
import net.wrightnz.android.three.geom.Vertex;
import net.wrightnz.android.three.geom.ZProjector;
import net.wrightnz.android.three.image.EdgeEnhanceFilter;
import net.wrightnz.android.utils.ImageUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Richard Wright (Spark NZ Ventures) on 26/06/17.
 *
 * @author Richard Wright
 */
public class Object3DGenerator {

    private static final String TAG = Object3DGenerator.class.getSimpleName();

    public static Bitmap findEdges(Uri uri) throws IOException {
        Log.d(TAG, "Called: findEdges(" + uri + ")");
        Bitmap bitmap = ImageUtils.loadImage(uri);
        int[] pixels = ImageUtils.getPixels(bitmap);
        EdgeEnhanceFilter filter = new EdgeEnhanceFilter();
        int[] result = filter.filterSimple(pixels);
        return ImageUtils.createBitmap(result, bitmap.getWidth(), bitmap.getHeight());
    }

    public static Object3d getObject3dFromImage(Bitmap bitmap) {
        Log.d(TAG, "Called: getObjectFromImage(" + bitmap + ")");
        Object3d obj = new Object3d();
        List<Vertex> orderedVertices = orderVertices(getUnorderedVertices(bitmap));
        /* Group group = new Group("Group");
        Face face = new Face();
        face.setVertices(orderedVertices);
        group.addFace(face);
        obj.addGroup(group); */

        List<Vertex> vertices = new ArrayList<>();

        ZProjector projector = new ZProjector(1);

        Face forwardface = projector.projectZ(vertices, orderedVertices, 0.1f);
        Group forwardGroup = new Group("ForwardGroup");
        forwardGroup.addFace(forwardface);
        obj.addGroup(forwardGroup);

        Face rearFaces = projector.projectZ(vertices, orderedVertices, -0.1f);
        Group rearGroup = new Group("RearGroup");
        rearGroup.addFace(rearFaces);
        obj.addGroup(rearGroup);

        obj.setVertices(vertices);

        FaceJoiner joiner = new FaceJoiner();
        forwardGroup.addFace(joiner.join(forwardface, rearFaces));

        Log.d(TAG, "OBJ is: " + obj);
        return obj;
    }

    private static List<Vertex> orderVertices(List<Vertex> vertices) {
        BoundingBox bounds = new BoundingBox(vertices);
        float centreX = bounds.getCenter().getX();
        List<Vertex> orderedVertices = new ArrayList<>();
        int order = 0;
        for (Vertex vertex : vertices) {
            // if (vertex.getX() > centreX) {
                vertex.setIndex(++order);
                orderedVertices.add(vertex);
            // }
        }
        /*for (Vertex vertex : Reverser.reversed(vertices)) {
            if (vertex.getX() <= centreX) {
                vertex.setIndex(++order);
                orderedVertices.add(vertex);
            }
        }*/
        return orderedVertices;
    }

    private static List<Vertex> getUnorderedVertices(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int rowSpacing = height / 15;
        int[][] pixels = ImageUtils.one2Two(ImageUtils.getPixels(bitmap), width, height);
        List<Vertex> rawVertices = new ArrayList<>();
        for (int i = rowSpacing; i < pixels.length; i = i + rowSpacing) {
            int[] currentRow = pixels[i];
            for (int x = 0; x < currentRow.length - 1; x++) {
                int pixel = currentRow[x];
                int pixelBelow = pixels[i + 1][x];
                int pixelAbove = pixels[i - 1][x];
                int nextPixel = currentRow[x + 1];
                if (pixel == Color.WHITE && pixelBelow == Color.WHITE && pixelAbove == Color.WHITE) {
                    Vertex point = new Vertex(((float) x / (float) width), ((float) i / (float) width), 0);
                    rawVertices.add(point);
                    break;
                }
            }
        }
        return rawVertices;
    }

}
