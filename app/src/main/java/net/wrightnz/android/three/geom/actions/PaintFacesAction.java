/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.wrightnz.android.three.geom.actions;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import net.wrightnz.android.three.geom.Face;
import net.wrightnz.android.three.geom.Object3d;
import net.wrightnz.android.three.geom.Vertex;
import net.wrightnz.android.utils.graphics.DrawUtils;

import java.util.List;


/**
 *
 * @author Richard Wright
 */
public class PaintFacesAction implements Action {

    private final Object3d object;
    private final Canvas canvas;
    private final int selectionHandleSize;

    public PaintFacesAction(Object3d object, Canvas canvas, int selectionHandleSize){
        this.object = object;
        this.canvas = canvas;
        this.selectionHandleSize = selectionHandleSize;
    }

    @Override
    public List<Face> performAction(List<Face> faces, Paint paint) {
        Paint red = new Paint(paint);
        red.setColor(Color.RED);
        for (Face face : faces) {
            List<Vertex> vertices = face.getVertices();
            int[] xis = new int[vertices.size()];
            int[] yis = new int[vertices.size()];
            int i = 0;
            for (Vertex vertex : vertices) {
                int vertexIndex = vertex.getIndex() - 1;
                xis[i] = (int) object.getTransformedVertices().get(vertexIndex).getX();
                yis[i] = (int) object.getTransformedVertices().get(vertexIndex).getY();
                Vertex v = object.getVertices().get(vertexIndex);
                DrawUtils.fillOval(canvas, xis[i] - 4
                        , yis[i] - 4, 8, 8, paint);
                if (v.isSelected()) {
                    int halfHandle = (selectionHandleSize / 2);
                    DrawUtils.fillOval(canvas, xis[i] - halfHandle, yis[i] - halfHandle, selectionHandleSize, selectionHandleSize, red);
                }
                i++;
            }
            if (face.isSelected()) {
                DrawUtils.fillPolygon(canvas, xis, yis, vertices.size(), paint);
            } else {
                DrawUtils.drawPolygon(canvas, xis, yis, vertices.size(), paint);
            }
        }
        return faces;
    }

}
