/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.wrightnz.android.three.geom;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import androidx.annotation.NonNull;

import net.wrightnz.android.three.geom.actions.Action;
import net.wrightnz.android.three.geom.actions.DeleteVerticesFromFacesAction;
import net.wrightnz.android.three.geom.actions.FaceActioner;
import net.wrightnz.android.three.geom.actions.FaceSelectAction;
import net.wrightnz.android.three.geom.actions.PaintFacesAction;
import net.wrightnz.android.utils.graphics.DrawUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Encapsulates a three dimensional object made up of vertices and groups of faces.
 *
 * @author Richard Wright, richard@wrightnz.net
 */
public class Object3d implements Parcelable {

    public static final int DRAW_MODE_PLAIN = 0;
    public static final int DRAW_MODE_GROUPS = 1;
    public static final int DRAW_MODE_MATERIALS = 2;

    private static final String TAG = Object3d.class.getSimpleName();

    private List<Vertex> vertices = new ArrayList<>();
    private List<Group> groups = new ArrayList<Group>();
    private List<Vertex> transformedVertices = new ArrayList<Vertex>();

    private int[] gradient;
    private static final int[] groupColours = {Color.GREEN, Color.BLUE, Color.CYAN,
            Color.GRAY, Color.MAGENTA, Color.RED,
            Color.WHITE, Color.WHITE, Color.YELLOW,
            Color.LTGRAY};

    private int drawMode = DRAW_MODE_PLAIN;

    private int selectionHandleSize;

    private final Map<String, Integer> materialMap = new HashMap<>();

    public Object3d() {
        gradient = GeomUtils.createGreenGradient();
    }

    protected Object3d(Parcel in) {
        vertices = in.createTypedArrayList(Vertex.CREATOR);
        groups = in.createTypedArrayList(Group.CREATOR);
        transformedVertices = in.createTypedArrayList(Vertex.CREATOR);
        gradient = in.createIntArray();
        drawMode = in.readInt();
        selectionHandleSize = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(vertices);
        dest.writeTypedList(groups);
        dest.writeTypedList(transformedVertices);
        dest.writeIntArray(gradient);
        dest.writeInt(drawMode);
        dest.writeInt(selectionHandleSize);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Object3d> CREATOR = new Creator<Object3d>() {
        @Override
        public Object3d createFromParcel(Parcel in) {
            return new Object3d(in);
        }

        @Override
        public Object3d[] newArray(int size) {
            return new Object3d[size];
        }
    };

    /**
     * @return the vertices
     */
    public List<Vertex> getVertices() {
        return vertices;
    }

    /**
     * @return a list containing only the Vertices currently Selected.
     */
    public List<Vertex> getSelectedVertices() {
        List<Vertex> selectedVerts = new ArrayList<>();
        for (Vertex vert : vertices) {
            if (vert.isSelected()) {
                selectedVerts.add(vert);
            }
        }
        return selectedVerts;
    }

    /**
     * @param vertices the vertices to set
     */
    public void setVertices(List<Vertex> vertices) {
        this.vertices = vertices;
    }


    /**
     * Move all vertice with isSelect true. The new position of the vertices
     * being there current possition plus deltas. The X, Y and Z deltas being
     * the X, Y and Z values of the input point. i.e. the input point isn't
     * really a point it's a delta to the selected points.
     *
     * @param move point describing the difference to move.
     */
    public void moveSelectedVertices(Point3D move) {
        for (Vertex vert : vertices) {
            if (vert.isSelected()) {
                vert.add(move);
            }
        }
    }

    public void wieldSelected() {
        List<Vertex> selectedVerts = getSelectedVertices();
        Vertex firstVertex = selectedVerts.get(0);
        for (Vertex vertex : selectedVerts) {
            vertex.setX(firstVertex.getX());
            vertex.setY(firstVertex.getY());
            vertex.setZ(firstVertex.getZ());
        }
    }

    public void addPolygon() {
        List<Vertex> selectedVerts = getSelectedVertices();
        if (selectedVerts.size() != 2) {
            throw new RuntimeException("Two vertices must be selected");
        }

        Vertex vertOne = selectedVerts.get(0);
        Vertex vertTwo = selectedVerts.get(1);
        boolean isVertical = (vertOne.getX() - vertTwo.getX() == 0) && (vertOne.getZ() - vertTwo.getZ() == 0);
        Vertex newVertOne;
        Vertex newVertTwo;
        if (isVertical) {
            newVertOne = new Vertex(vertOne.getX() + 0.1F, vertOne.getY(), vertOne.getZ(), vertices.size() + 1);
            vertices.add(newVertOne);
            newVertTwo = new Vertex(vertTwo.getX() + 0.1F, vertTwo.getY(), vertTwo.getZ(), vertices.size() + 1);
            vertices.add(newVertTwo);
        } else {
            newVertOne = new Vertex(vertOne.getX(), vertOne.getY() + 0.1F, vertOne.getZ(), vertices.size() + 1);
            vertices.add(newVertOne);
            newVertTwo = new Vertex(vertTwo.getX(), vertTwo.getY() + 0.1F, vertTwo.getZ(), vertices.size() + 1);
            vertices.add(newVertTwo);
        }

        Face newFace = new Face();
        newFace.addVertex(vertOne);
        newFace.addVertex(newVertOne);
        newFace.addVertex(newVertTwo);
        newFace.addVertex(vertTwo);
        GroupOfFaces groupOfFaces = getGroupForVertices(selectedVerts);
        if (groupOfFaces != null) {
            System.out.println("addPolygon() groupOfFaces = " + groupOfFaces.getName());
            List<Face> faces = groupOfFaces.getFaces();
            faces.add(newFace);
            groupOfFaces.setFaces(faces);
        }
    }

    public GroupOfFaces getGroupForVertices(List<Vertex> verts) {
        for (Group group : groups) {
            if (GeomUtils.groupContainsVertices(group, verts)) {
                return group;
            }
            List<MaterialGroup> materialGroups = group.getMaterialGroups();
            for (MaterialGroup materialGroup : materialGroups) {
                if (GeomUtils.groupContainsVertices(materialGroup, verts)) {
                    return materialGroup;
                }
            }
        }
        return groups.get(0);
    }


    /**
     * Delete the selected vertices from if the occur within the faces in this
     * Object3Ds group lists. Both Groups and MaterialGroups are handled.
     */
    public void deleteSelectedVertices() {
        List<Vertex> remainingVerts = new ArrayList<>();
        List<Vertex> deletedVertices = new ArrayList<>();
        for (Vertex vert : getVertices()) {
            if (vert.isSelected()) {
                deletedVertices.add(vert);
            } else {
                remainingVerts.add(vert);
            }
        }
        Action deleteVertsAction = new DeleteVerticesFromFacesAction(deletedVertices);
        FaceActioner.performAction(this, deleteVertsAction, null);
        this.setVertices(remainingVerts);
        this.reindex();
    }


    public void reindex() {
        int i = 0;
        for (Vertex vertex : vertices) {
            i++;
            vertex.setIndex(i);
        }
    }

    public void selectVertices(RectF selection) {
        for (Vertex vertex : getVertices()) {
            vertex.setSelected(false);
            float x = getTransformedVertices().get(vertex.getIndex() - 1).getX();
            float y = getTransformedVertices().get(vertex.getIndex() - 1).getY();
            if (selection.contains(x, y)) {
                vertex.setSelected(true);
            }
        }
        List<Face> selectedFaces = new ArrayList<Face>();
        selectedFaces.add(GeomUtils.makeFaceFromVertices(getSelectedVertices()));
        Action action = new FaceSelectAction(selectedFaces);
        FaceActioner.performAction(this, action, null);
    }

    /**
     * @param point
     * @return a reference to the Vertex selected.
     */
    public Vertex selectVertex(Point point) {
        Vertex selectVertex = null;
        for (Vertex vertex : getVertices()) {
            vertex.setSelected(false);
            int x = (int) getTransformedVertices().get(vertex.getIndex() - 1).getX();
            int y = (int) getTransformedVertices().get(vertex.getIndex() - 1).getY();
            Point tVertPoint = new Point(x, y);
            if (GeomUtils.isCloseEnough(point, tVertPoint) && selectVertex == null) {
                vertex.setSelected(true);
                selectVertex = vertex;
            }
        }
        return selectVertex;
    }

    /**
     * Add a vertex to this 3D Object.
     *
     * @param vertex
     */
    public void addVertex(Vertex vertex) {
        this.vertices.add(vertex);
    }

    /**
     * Remove a vertex from this 3D Object.
     *
     * @param vertex
     */
    public void removeVertex(Vertex vertex) {
        this.vertices.remove(vertex);
    }


    /**
     * @return the transformedVertices
     */
    public List<Vertex> getTransformedVertices() {
        return transformedVertices;
    }

    /**
     * @param transformedVertices the transformedVertices to set
     */
    public void setTransformedVertices(List<Vertex> transformedVertices) {
        this.transformedVertices = transformedVertices;
    }

    /**
     * Transform all the points in this model using the transformations
     * in the input matrix.
     */
    public void transform(Matrix3D matrix) {
        setTransformedVertices(matrix.transform(getVertices()));
    }

    public void resetTransformedVertices() {
        setTransformedVertices(new ArrayList<Vertex>());
        for (Vertex vertex : getVertices()) {
            getTransformedVertices().add(vertex);
        }
    }

    /**
     * Paint this Object3D to the Canvas.
     */
    public void paint(Canvas canvas, Paint paint) {
        if (getVertices().isEmpty()) {
            return;
        }
        if (drawMode == DRAW_MODE_GROUPS) {
            /*Paint grouped faces*/
            paintGroups(canvas, paint);
        } else if (drawMode == DRAW_MODE_MATERIALS) {
            /*Paint Material faces*/
            paintMaterials(canvas, paint);
        } else if (drawMode == DRAW_MODE_PLAIN) {
            paintMesh(canvas, paint);
        }
    }

    private void paintMesh(Canvas canvas, Paint paint) {
        Action action = new PaintFacesAction(this, canvas, selectionHandleSize);
        FaceActioner.performAction(this, action, paint);
    }

    private void paintGroups(Canvas canvas, Paint paint) {
        int i = 0;
        Action action = new PaintFacesAction(this, canvas, selectionHandleSize);
        for (Group group : groups) {
            // Paint faces not assigned a material.
            action.performAction(group.getFaces(), paint);
            for (MaterialGroup mg : group.getMaterialGroups()) {
                action.performAction(mg.getFaces(), paint);
            }
            i++;
            if (i >= groupColours.length) {
                i = 0;
            }
        }
    }

    private void paintMaterials(Canvas canvas, Paint paint) {
        Log.d(TAG, "Called: paintMaterials()");
        if (materialMap.isEmpty()) {
            loadMaterialMap();
        }
        Action action = new PaintFacesAction(this, canvas, selectionHandleSize);
        for (Group group : groups) {
            /* Paint faces not assigned a material */
            action.performAction(group.getFaces(), paint);
            for (MaterialGroup mg : group.getMaterialGroups()) {
                Integer colour = materialMap.get(mg.getMaterialName());
                if (colour != null) {
                    paint.setColor(colour);
                    action.performAction(mg.getFaces(), paint);
                }
            }
        }
    }

    private void loadMaterialMap() {
        int i = 0;
        for (Group group : groups) {
            for (MaterialGroup mg : group.getMaterialGroups()) {
                if (materialMap.get(mg.getMaterialName()) == null) {
                    materialMap.put(mg.getMaterialName(), groupColours[i]);
                    i++;
                    if (i >= groupColours.length) {
                        i = 0;
                    }
                }
            }
        }
    }

    /**
     * @return the groups
     */
    public List<Group> getGroups() {
        return groups;
    }

    /**
     * @param groups the groups to set
     */
    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    /**
     * @param group the Group to set
     */
    public void addGroup(Group group) {
        this.groups.add(group);
    }

    /**
     * @return the gradient
     */
    public int[] getGradient() {
        return gradient;
    }

    /**
     * @param gradient the gradient to set
     */
    public void setGradient(int[] gradient) {
        this.gradient = gradient;
    }


    public void setDrawMode(int drawMode) {
        this.drawMode = drawMode;
    }

    /**
     * @return the current drawing mode: DRAW_MODE_GROUPS, DRAW_MODE_MATERIALS, DRAW_MODE_PLAIN
     */
    public int setDrawMode() {
        return this.drawMode;
    }

    @NonNull
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("# Saved using Three D Android App by Richard Wright\n");
        sb.append("\n");
        for (Vertex v : vertices) {
            sb.append(v);
            sb.append("\n");
        }
        sb.append("\n\n");
        for (Group group : groups) {
            sb.append(group);
        }
        return sb.toString();
    }

    public int getSelectionHandleSize() {
        return this.selectionHandleSize;
    }

    public void setSelectionHandleSize(int selectionHandleSize) {
        this.selectionHandleSize = selectionHandleSize;
    }
}
