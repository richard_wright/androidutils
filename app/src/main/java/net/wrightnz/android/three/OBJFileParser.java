/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.wrightnz.android.three;

import net.wrightnz.android.three.geom.Face;
import net.wrightnz.android.three.geom.Group;
import net.wrightnz.android.three.geom.MaterialGroup;
import net.wrightnz.android.three.geom.Object3d;
import net.wrightnz.android.three.geom.Vertex;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;


/**
 * @author richardw
 */
public class OBJFileParser {

    private Object3d object = new Object3d();
    private Group group;
    private MaterialGroup materialGroup;

    public OBJFileParser() {
    }

    public void parse(File file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        try {
            parse(reader);
        } finally {
            reader.close();
        }
    }

    public void parse(BufferedReader reader) throws IOException {
        int numberOfVertices = 0;
        while (true) {
            String line = reader.readLine();
            if (line == null) {
                if (group != null) {
                    if (materialGroup != null) {
                        group.addMaterialGroup(materialGroup);
                    }
                    object.addGroup(group);
                }
                break;
            }
            line = line.trim();
            if (line.startsWith("g ")) {
                if (group != null) {
                    if (materialGroup != null) {
                        group.addMaterialGroup(materialGroup);
                        materialGroup = null;
                    }
                    object.addGroup(group);
                }
                group = new Group();
                addGroupName(line);
            } else if (line.startsWith("usemtl ")) {
                if (group != null && materialGroup != null) {
                    group.addMaterialGroup(materialGroup);
                }
                materialGroup = new MaterialGroup();
                addMaterialName(line);
            } else if (line.startsWith("f ")) {
                if (group != null && materialGroup == null) {
                    addFaceToGroup(line);
                } else if (group != null && materialGroup != null) {
                    addFaceToMaterialGroup(line);
                }
            } else if (line.startsWith("v ")) {
                numberOfVertices++;
                addVertex(line, numberOfVertices);
            } else if (line.startsWith("fo ")) {
                if (group != null && materialGroup == null) {
                    addFaceToGroup(line);
                } else if (group != null && materialGroup != null) {
                    addFaceToMaterialGroup(line);
                }
            } else if (line.startsWith("l ")) {
                if (group != null && materialGroup == null) {
                    addFaceToGroup(line);
                } else if (group != null && materialGroup != null) {
                    addFaceToMaterialGroup(line);
                }
            }
        }
    }


    /**
     * @return the model
     */
    public Object3d getObject() {
        return this.object;
    }

    /**
     * Add a Group Name to the 3D Object.
     * <p>
     * Only 1 group name is currently supported :(
     */
    public void addGroupName(String line) {
        StringTokenizer st = new StringTokenizer(line);
        if (st.countTokens() >= 2) {
            String type = st.nextToken();
            String name = st.nextToken();
            group.setGroupName(name);
        } else {
            group.setGroupName("default");
        }
    }

    public void addMaterialName(String line) {
        StringTokenizer st = new StringTokenizer(line);
        String type = st.nextToken();
        String name = st.nextToken();
        materialGroup.setMaterialName(name);
    }


    /**
     * Add a Vertex to this list of Vertices
     */
    public void addVertex(String line, int index) {
        StringTokenizer st = new StringTokenizer(line);
        String type = st.nextToken();
        float x = Float.valueOf(st.nextToken());
        float y = Float.valueOf(st.nextToken());
        float z = Float.valueOf(st.nextToken());
        Vertex vertex = new Vertex(x, y, z, index);
        object.addVertex(vertex);
    }

    public void addFaceToGroup(String line) {
        Face face = new Face();
        StringTokenizer st = new StringTokenizer(line);
        String type = st.nextToken();
        while (st.hasMoreTokens()) {
            String vertIndex = getVertIndex(st.nextToken());
            int point = Integer.parseInt(vertIndex);
            Vertex vertex = object.getVertices().get(point - 1);
            vertex.setIndex(point);
            face.addVertex(vertex);
        }
        group.addFace(face);
    }

    private void addFaceToMaterialGroup(String line) {
        Face face = new Face();
        StringTokenizer st = new StringTokenizer(line);
        String type = st.nextToken();
        while (st.hasMoreTokens()) {
            String vertIndex = getVertIndex(st.nextToken());
            int point = Integer.parseInt(vertIndex);
            Vertex vertex = object.getVertices().get(point - 1);
            vertex.setIndex(point);
            face.addVertex(vertex);
        }
        materialGroup.addFace(face);
    }


    private String getVertIndex(String str) {
        int indexOfSlash = str.indexOf("/");
        if (indexOfSlash > -1) {
            return str.substring(0, indexOfSlash);
        } else {
            return str;
        }

    }

}
