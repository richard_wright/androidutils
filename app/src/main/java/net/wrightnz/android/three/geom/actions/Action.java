/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.wrightnz.android.three.geom.actions;

import android.graphics.Paint;

import net.wrightnz.android.three.geom.Face;

import java.util.List;


/**
 *
 * @author Richard Wright
 */
public interface Action {

    List<Face> performAction(List<Face> faces, Paint paint);

}
