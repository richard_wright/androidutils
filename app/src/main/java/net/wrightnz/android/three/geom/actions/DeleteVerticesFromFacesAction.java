/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.wrightnz.android.three.geom.actions;

import android.graphics.Paint;

import net.wrightnz.android.three.geom.Face;
import net.wrightnz.android.three.geom.Vertex;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Richard Wright
 */
public class DeleteVerticesFromFacesAction implements Action{
    
    private List<Vertex> deleteVertices;

    /**
     * Remove the specified deleted Vertices from a list of Faces
     * @param deleteVertices
     */
    public DeleteVerticesFromFacesAction(List<Vertex> deleteVertices){
        this.deleteVertices = deleteVertices;
    }

    /**
     * Remove the deleted Vertices past in on the constructor from a
     * List of Faces
     * @param faces
     * @return List of faces with the specified vertices removed.
     */
    @Override
    public List<Face> performAction(List<Face> faces, Paint paint) {
        return deleteFromFaces(faces, deleteVertices);
    }


   private List<Face> deleteFromFaces(List<Face> currentFaces, List<Vertex> deletedVertices){
        List<Face> newFaces = new ArrayList<Face>();
        for(Face face : currentFaces){
            List<Vertex> newVerts = new ArrayList<Vertex>();
            for (Vertex vert : face.getVertices()) {
                if (!deletedVertices.contains(vert)) {
                    newVerts.add(vert);
                }
            }
            face.setVertices(newVerts);
            newFaces.add(face);
        }
        return newFaces;
    }

}
