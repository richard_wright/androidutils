/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.wrightnz.android.three.geom.actions;

import android.graphics.Paint;

import net.wrightnz.android.three.geom.Face;

import java.util.ArrayList;
import java.util.List;


/**
 * Action Implementation which deletes a face from the specified list of faces.
 * The case will be deleted if it is specified on the contructor or has a state
 * of isSelected.
 *
 * @author Richard Wright
 */
public class DeleteFaceAction implements Action {

    private Face deleteFace;

    public DeleteFaceAction(Face deleteFace) {
        this.deleteFace = deleteFace;
    }

    @Override
    public List<Face> performAction(List<Face> faces, Paint paint) {
        List<Face> newFaces = new ArrayList<Face>();
        for (Face face : faces) {
            if (!face.equals(deleteFace)) {
                newFaces.add(face);
            } else if (!face.isSelected()) {
                newFaces.add(face);
            }
        }
        return newFaces;
    }

}
