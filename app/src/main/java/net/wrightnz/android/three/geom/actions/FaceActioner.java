/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.wrightnz.android.three.geom.actions;


import android.graphics.Paint;

import net.wrightnz.android.three.geom.Face;
import net.wrightnz.android.three.geom.Group;
import net.wrightnz.android.three.geom.MaterialGroup;
import net.wrightnz.android.three.geom.Object3d;

import java.util.List;

/**
 * @author Richard Wright
 *         <p>
 *         Class to iterate through all the faces in the Object3D and perform some
 *         action on them.
 */
public class FaceActioner {

    public static void performAction(Object3d object, Action action, Paint paint) {
        List<Group> groups = object.getGroups();
        for (Group group : groups) {
            List<Face> faces = group.getFaces();
            group.setFaces(action.performAction(faces, paint));
            List<MaterialGroup> materialGroups = group.getMaterialGroups();
            for (MaterialGroup materialGroup : materialGroups) {
                List<Face> materialFaces = materialGroup.getFaces();
                materialGroup.setFaces(action.performAction(materialFaces, paint));
            }
        }
    }
}
