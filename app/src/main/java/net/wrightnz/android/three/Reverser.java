package net.wrightnz.android.three;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Reverser<T> implements Iterable<T> {
    private final List<T> original;

    public Reverser(List<T> original) {
        this.original = original;
    }

    public Iterator<T> iterator() {
        final ListIterator<T> i = original.listIterator(original.size());

        return new Iterator<T>() {
            public boolean hasNext() { return i.hasPrevious(); }
            public T next() { return i.previous(); }
            public void remove() { i.remove(); }
        };
    }

    public static <T> Reverser<T> reversed(List<T> original) {
        return new Reverser<T>(original);
    }
}