package net.wrightnz.android.three.database;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created for (Spark NZ) on 20/12/17.
 *
 * @author Richard Wright
 */

public class Cache implements Parcelable {

    private long id;
    private String name;

    public Cache() {
    }

    protected Cache(Parcel in) {
        id = in.readLong();
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Cache> CREATOR = new Creator<Cache>() {
        @Override
        public Cache createFromParcel(Parcel in) {
            return new Cache(in);
        }

        @Override
        public Cache[] newArray(int size) {
            return new Cache[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(Cache.class.getSimpleName());
        sb.append("{");
        sb.append("id: ").append(id);
        sb.append(", name: ").append(name);
        sb.append("}");
        return sb.toString();
    }
}
