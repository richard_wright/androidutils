/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.wrightnz.android.three.geom;

import android.os.Parcel;
import android.os.Parcelable;

/**
 *
 * @author Richard Wright
 */
public class Vertex extends Point3D implements Parcelable {

    /**
     * This is the number order of this Vertex in the OBJ file.
     */
    private int index;

    /**
     * A vertex may be select. i.e. flagged to have some action performed on it.
     */
    private boolean selected;

    public Vertex() {
    }

    protected Vertex(Parcel in) {
        super(in);
        index = in.readInt();
        selected = (in.readByte() == 1);
    }

    public Vertex(Vertex vertex){
        super.copy(vertex);
        this.setSelected(vertex.isSelected());
        this.index = vertex.getIndex();
    }

    public Vertex(float x, float y, float z){
        super(x, y, z);
    }

    public Vertex(Point3D point){
        super(point);
    }

    public Vertex(float x, float y, float z, int index){
        super(x, y, z);
        this.index = index;
    }

    public Vertex(Point3D point, int index){
        super(point);
        this.index = index;
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(index);
        parcel.writeByte(selected ? (byte) 0 : (byte) 1);
    }

    /**
     * To vertices are considered equal if they have the same index
     *
     * @param vertex
     * @return true of vertex is equal to this vertex as defined above, otherwise
     *         false.
     */
    public boolean equals(Vertex vertex) {
        if(vertex == null){
            return false;
        }
        else{
            return (this.getIndex() == vertex.getIndex());
        }
    }


    /**
     * To vertices are considered equal if they have the same index
     *
     * @param obj
     * @return true of vertex is equal to this vertex as defined above, otherwise
     *         false.
     */
    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        else if(!(obj instanceof Vertex)){
            return false;
        }
        else{
            return equals((Vertex)obj);
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.index;
        return hash;
    }

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public Vertex setIndex(int index) {
        this.index = index;
        return this;
    }


    /**
     * @return the selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("v ");
        sb.append(this.getX());
        sb.append(" ");
        sb.append(this.getY());
        sb.append(" ");
        sb.append(this.getZ());
        return sb.toString();
    }


    public static final Creator<Vertex> CREATOR = new Creator<Vertex>() {
        @Override
        public Vertex createFromParcel(Parcel in) {
            return new Vertex(in);
        }

        @Override
        public Vertex[] newArray(int size) {
            return new Vertex[size];
        }
    };

}
