/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.wrightnz.android.three.geom;

/**
 *
 * @author Richard Wright
 */
public class Edge {

    private Vertex v1;
    private Vertex v2;

    /**
     * @param v1 the first Vertex defining this edge
     * @param v2 the second Vertex defining this edge
     */
    public Edge(Vertex v1, Vertex v2){
        this.v1 = v1;
        this.v2 = v2;
    }

    /**
     * @return the v2
     */
    public Vertex getVertex2() {
        return v2;
    }

    /**
     * @param v2 the v2 to set
     */
    public void setVertex2(Vertex v2) {
        this.v2 = v2;
    }

    /**
     * 
     * @return the vertex 1
     */
    public Vertex getVertex1() {
        return v2;
    }

    /**
     * @param v1 the first Vertex defining this edge to set
     */
    public void setVertex1(Vertex v1) {
        this.v1 = v1;
    }

    @Override
    public String toString(){
        return "Edge [(" + v1 + "), (" + v2 + ")]";
    }

}
