package net.wrightnz.android.three.geom;

import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;

import java.util.List;

/**
 * Created by Richard Wright (Spark NZ Ventures) on 14/03/17.
 *
 * Utility methods for geometric operations on shapes.
 *
 * @author Richard Wright
 */
public final class GeomUtils {

    private static final String TAG = GeomUtils.class.getSimpleName();

    /**
     *
     * @param selection the rectangle to translate
     * @param x the amount to translate in the x direction
     * @param y the amount to translate in the y direction
     * @param bounds the maximum allowed coordinate space.
     * @return
     */
    public static Rect translate(Rect selection, float x, float y, RectF bounds) {
        // Log.d(TAG, "Called: translate(" + rectangle + ", x:" + x + ", y: " + y + ", bounds: " + bounds + ")");
        float newLeft = selection.left + x;
        float newRight = selection.right + x;
        float newTop = selection.top + y;
        float newBottom = selection.bottom + y;
        if (newLeft < bounds.left) {
            newLeft = bounds.left;
        }
        if (newTop < bounds.top) {
            newTop = bounds.top;
        }
        float maxLeft = bounds.right - selection.width();
        float maxTop = bounds.bottom - selection.height();

        if (newLeft > maxLeft) {
            newLeft = maxLeft;
        }
        if (newTop > maxTop) {
            newTop = maxTop;
        }
        Rect result = new Rect((int) newLeft, (int) newTop, (int) newRight, (int) newBottom);
        if (result.width() != selection.width() || result.height() != selection.height()) {
            // Translate cannot change the size if the rectangle if it has we've moved to far
            // so the original rectangle should be returned.
            return selection;
        }
        // Log.d(TAG, "Translation result is: " + result);
        return result;
    }

    /**
     * @param center the point to be zoomed in to, or out from.
     * @param level  the level of zoom 1 to 100 where 1 is full size (the result is the bounds)
     *               and 100 the result is the smallest fraction of the bounds possible without
     *               "flipping" the bounds (i.e. the bounds going into negative space).
     * @param bounds the maximum allowed coordinate space.
     * @return
     */
    public static Rect zoom(Point center, int level, RectF bounds) {
        Log.d(TAG, "Called: zoom(" + center + ", level: " + level + ", bounds: " + bounds + ")");
        Rect result = new Rect((int) bounds.left, (int) bounds.top, (int) bounds.right, (int) bounds.bottom);
        if (level == 0) {
            return result;
        } else {
            float xStepSize = (bounds.width() / 200.0f);
            float yStepSize = (bounds.height() / 200.0f);
            float dx = (xStepSize * level);
            float dy = (yStepSize * level);

            result.offset((int) (center.x - (result.width() / 2.0f)), (int) (center.y - (result.height() / 2.0f)));
            result.inset((int) dx, (int) dy);
            return result;
        }
    }

    /**
     * @param value
     * @param tolerance
     * @return true if the value is within +/-tolerance of zero.
     */
    public static boolean isAlmostNothing(float value, float tolerance) {
        return (value > -tolerance && value < tolerance);
    }

    /**
     * @param p1
     * @param p2
     * @return true if and only if p1 and p2 are withing 5 pixels of
     * each other in both x and y axises.
     */
    public static boolean isCloseEnough(Point p1, Point p2) {
        int xDelta = (int) Math.abs(p1.x - p2.x);
        int yDelta = (int) Math.abs(p1.y - p2.y);
        if (xDelta < 5 && yDelta < 5) {
            return true;
        } else {
            return false;
        }
    }

    public static int[] createGreenGradient() {
        int[] gradient = new int[16];
        for (int i = 0; i < gradient.length; i++) {
            int green = (int) (170 * (1 - Math.pow(i / 15.0, 2.3)) + 55);
            gradient[i] = Color.argb(255, 0, green, 0);
        }
        return gradient;
    }


    /**
     *
     * @param vertices
     * @return face made up of the input vertices.
     */
    public static Face makeFaceFromVertices(List<Vertex> vertices){
        Face face = new Face();
        for (Vertex vertex : vertices) {
            face.addVertex(vertex);
        }
        return face;
    }

    /**
     * Flip around a face so that it's face side is facing the other way.
     *
     * @param face to flip
     * @return the flipped over Face.
     */
    public static Face flipFace(Face face) {
        Face flippedFace = new Face();
        List<Vertex> verts = face.getVertices();
        for (int i = verts.size() - 1; i >= 0; i--) {
            flippedFace.addVertex(verts.get(i));
        }
        return flippedFace;
    }

    public static boolean groupContainsVertices(GroupOfFaces group, List<Vertex> verts){
        boolean[] results = new boolean[verts.size()];
        List<Face> faces = group.getFaces();
        for(Face face : faces){
            List<Vertex> faceVerts = face.getVertices();
            int i = 0;
            for(Vertex vertex : verts){
                if(faceVerts.contains(vertex)){
                    results[i] = true;
                    i++;
                }
            }
            for(int c = 0; c < results.length; c++){
                if(results[c] == false){
                    return false;
                }
            }
            return true;
        }
        return false;
    }


}
