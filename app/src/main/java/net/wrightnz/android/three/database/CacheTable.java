package net.wrightnz.android.three.database;

import android.content.ContentValues;
import android.database.Cursor;

import net.wrightnz.android.easydb.Column;
import net.wrightnz.android.easydb.Table;

/**
 * Created for (Spark NZ) on 20/12/17.
 *
 * @author Richard Wright
 */

public class CacheTable extends Table<Cache> {

    private static final Column[] COLUMNS = {
            COLUMN_ID,
            new Column("name", "TEXT")
    };

    @Override
    public Column[] getColumns() {
        return COLUMNS;
    }

    @Override
    public ContentValues toContentValues(Cache data) {
        ContentValues values = new ContentValues();
        values.put(COLUMNS[0].getName(), data.getId());
        values.put(COLUMNS[1].getName(), data.getName());
        return values;
    }

    @Override
    public Cache fromCursor(Cursor cursor) {
        Cache cache = new Cache();
        cache.setId(cursor.getInt(0));
        cache.setName(cursor.getString(1));
        return cache;
    }
}
