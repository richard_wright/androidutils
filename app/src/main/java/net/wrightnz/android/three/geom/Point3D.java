/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.wrightnz.android.three.geom;

import android.os.Parcel;
import android.os.Parcelable;

/**
 *
 * @author Richard Wright
 */
public class Point3D implements Parcelable {

    /*
     * Location of this Point in three dimensions
     */
    private float x;
    private float y;
    private float z;

    /**
     * Create a new three Dimension point at 0.0, 0.0, 0.0
     */
    public Point3D(){
    }

    /**
     * Create a new three Dimension point at the same location as point.
     * @param point
     */
    public Point3D(Point3D point){
        copy(point);
    }

    /**
     * Create a new three Dimension point at location x, y, z
     * @param x
     * @param y
     * @param z
     */
    public Point3D(float x, float y, float z){
        this.x= x;
        this.y = y;
        this.z = z;
    }

    protected Point3D(Parcel in) {
        x = in.readFloat();
        y = in.readFloat();
        z = in.readFloat();
    }

    public static final Creator<Point3D> CREATOR = new Creator<Point3D>() {
        @Override
        public Point3D createFromParcel(Parcel in) {
            return new Point3D(in);
        }

        @Override
        public Point3D[] newArray(int size) {
            return new Point3D[size];
        }
    };

    /**
     * @return the x
     */
    public float getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(float x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public float getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(float y) {
        this.y = y;
    }

    /**
     * @return the z
     */
    public float getZ() {
        return z;
    }

    /**
     * @param z the z to set
     */
    public void setZ(float z) {
        this.z = z;
    }

    public void copy(Point3D point) {
        this.x = point.getX();
        this.y = point.getY();
        this.z = point.getZ();
    }

    public void add(Point3D point) {
        this.x += point.getX();
        this.y += point.getY();
        this.z += point.getZ();
    }

    public void subtract(Point3D point) {
        x -= point.getX();
        y -= point.getY();
        z -= point.getX();
    }

    public void cmul(Point3D point) {
        x *= point.getX();
        y *= point.getY();
        z *= point.getZ();
    }

    public float dot(Point3D point) {
        return x * point.getX() + y * point.getY() + z * point.getZ();
    }

    public void mul(Point3D point) {
        float tx = x, ty = y, tz = z;
        x = ty * point.z - tz * point.y;
        y = tz * point.x - tx * point.z;
        z = tx * point.y - ty * point.x;
    }

    public void mul(float f) {
        x *= f;
        y *= f;
        z *= f;
    }

    public void div(float f) {
        x /= f;
        y /= f;
        z /= f;
    }


    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        else if(!(obj instanceof Point3D)){
            return false;
        }
        else{
            return equals((Point3D)obj);
        }
    }

    /**
     * To points are considered equal if they occupy the same 3D point.
     *
     * @param point
     * @return true of point is equal to this point as defined above, otherwise
     *         false.
     */
    public boolean equals(Point3D point) {
        if (point == null) {
            return false;
        }
        return x == point.getX() && y == point.getY() && z == point.getZ();
    }

    public float vabs() {
        return (float) Math.sqrt(x * x + y * y + z * z);
    }

    public void normalize() {
        float t = vabs();
        x /= t;
        y /= t;
        z /= t;
    }

    public float cos(Point3D point) {
        return dot(point) / (vabs() * point.vabs());
    }

    public float sin(Point3D point) {
        Point3D t = new Point3D(this);
        t.mul(point);
        return t.vabs() / (vabs() * point.vabs());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(x);
        parcel.writeFloat(y);
        parcel.writeFloat(z);
    }
}
