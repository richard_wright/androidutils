package net.wrightnz.android.three.image;

import android.graphics.Color;
import android.graphics.Rect;

import net.wrightnz.android.utils.ImageUtils;

/**
 * Created by Richard Wright on 22/06/2017.
 */
public class EdgeEnhanceFilter {

    /**
     * Creates a new instance of EdgeEnhanceFilter
     */
    public EdgeEnhanceFilter() {
    }

    public int[] filterSimple(int[] pixels) {
        int[] result = new int[pixels.length];
        for (int i = 0; i < pixels.length - 2; i++) {
            int[] output = ImageUtils.edgeFinder(pixels[i], pixels[i + 1], pixels[i + 2]);
            result[i] = output[0];
            result[i + 1] = output[1];
            result[i + 2] = output[2];
        }
        return result;
    }

    public int[][] filter(int[][] pixels) {
        int width = pixels.length;
        int height = pixels[0].length;
        return filter(pixels, new Rect(0, 0, width, height));
    }

    public int[][] filter(int[][] pixels, Rect regionOfInterest) {
        int width = pixels.length;
        int height = pixels[0].length;
        int[][] eX = findEdgesX(pixels, regionOfInterest);
        int[][] eY = findEdgesY(pixels, regionOfInterest);
        int[][] combined = new int[width][height];
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                combined[x][y] = Math.max(eX[x][y], eY[x][y]);
            }
        }
        return combined;
    }

    public String getName() {
        return "Edge Enhance";
    }

    private int[][] findEdgesX(int[][] pix, Rect regionOfInterest) {
        for (int x = 0; x < (pix.length - 1); x = x + 2) {
            for (int y = 0; y < pix[0].length; y++) {
                if (regionOfInterest.contains(x, y)) {
                    int red1 = Color.red(pix[x][y]);
                    int red2 = Color.red(pix[x + 1][y]);

                    int green1 = Color.green(pix[x][y]);
                    int green2 = Color.green(pix[x + 1][y]);

                    int blue1 = Color.blue(pix[x][y]);
                    int blue2 = Color.blue(pix[x + 1][y]);

                    int alpha = Color.alpha(pix[x][y]);

                    int[] reds = ImageUtils.edgeEnhance(red1, red2);
                    int[] blues = ImageUtils.edgeEnhance(blue1, blue2);
                    int[] greens = ImageUtils.edgeEnhance(green1, green2);

                    pix[x][y] = (alpha << 24) | (reds[0] << 16) | (greens[0] << 8) | blues[0];
                    pix[x + 1][y] = (alpha << 24) | (reds[1] << 16) | (greens[1] << 8) | blues[1];
                }
            }
        }
        return pix;
    }

    private int[][] findEdgesY(int[][] pix, Rect regionOfInterest) {
        for (int x = 0; x < pix.length; x++) {
            for (int y = 0; y < (pix[0].length - 1); y = y + 2) {
                if (regionOfInterest.contains(x, y)) {
                    int red1 = Color.red(pix[x][y]);
                    int red2 = Color.red(pix[x][y + 1]);

                    int green1 = Color.green(pix[x][y]);
                    int green2 = Color.green(pix[x][y + 1]);

                    int blue1 = Color.blue(pix[x][y]);
                    int blue2 = Color.blue(pix[x][y + 1]);

                    int alpha = Color.alpha(pix[x][y]);

                    int[] reds = ImageUtils.edgeEnhance(red1, red2);
                    int[] blues = ImageUtils.edgeEnhance(blue1, blue2);
                    int[] greens = ImageUtils.edgeEnhance(green1, green2);

                    pix[x][y] = (alpha << 24) | (reds[0] << 16) | (greens[0] << 8) | blues[0];
                    pix[x][y + 1] = (alpha << 24) | (reds[1] << 16) | (greens[1] << 8) | blues[1];
                }
            }
        }
        return pix;
    }

}
