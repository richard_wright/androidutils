/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.wrightnz.android.three.geom.actions;

import android.graphics.Paint;

import net.wrightnz.android.three.geom.Face;

import java.util.List;

/**
 * Action implementation which selects the faces within an Object3D which equal
 * the list of faces past in on the constructor
 * @author Richard Wright
 */
public class FaceSelectAction implements Action {

    private List<Face> selectedFaces;

    public FaceSelectAction(List<Face> selectedFaces){
        this.selectedFaces = selectedFaces;
    }

    @Override
    public List<Face> performAction(List<Face> faces, Paint paint) {
        for(Face face : faces){
            face.setSelected(false);
            if(selectedFaces.contains(face)){
                face.setSelected(true);
            }
        }
        return faces;
    }

}
