package net.wrightnz.android.three.view;

import static net.wrightnz.android.three.geom.GeomUtils.isAlmostNothing;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.wrightnz.android.three.R;
import net.wrightnz.android.three.geom.BoundingBox;
import net.wrightnz.android.three.geom.Matrix3D;
import net.wrightnz.android.three.geom.Object3d;
import net.wrightnz.android.three.geom.Point3D;
import net.wrightnz.android.utils.LogUtils;
import net.wrightnz.android.utils.ViewUtils;

/**
 * Created on 26/05/17.
 *
 * @author Richard Wright
 */
public class ThreeDView extends View {

    public static final boolean IS_RELEASE = false;

    public static int MODE_ROTATE = 1;
    public static int MODE_SELECT = 3;

    private static final String TAG = ThreeDView.class.getSimpleName();

    private static final float STEPS_PER_SPIN = 500;
    private final Paint paint = new Paint();
    private final Paint selectionPaint = new Paint();
    private int selectionHandleSize;

    private Object3d object;
    private BoundingBox boundingBox;
    private RectF selection;
    private final Matrix3D matrix = new Matrix3D();
    private final Matrix3D transformMatrix = new Matrix3D();
    private float xfac = 1.0f;

    private int mode = MODE_ROTATE;
    private float totalDx;
    private float totalDy;
    private float xStep;
    private float yStep;
    private float unitScale = 0.0f;

    private GestureDetector gestureDetector;
    private ScaleGestureDetector scaleGestureDetector;

    private final GestureDetector.SimpleOnGestureListener gestureListener = new GestureDetector.SimpleOnGestureListener() {

        @Override
        public boolean onDown(@NonNull MotionEvent e) {
            selection = new RectF();
            totalDx = 0;
            totalDy = 0;
            postInvalidateOnAnimation();
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, @NonNull MotionEvent e2, float distanceX, float distanceY) {
            // Log.d(TAG, "Called: onScroll(scrollDeltaX: " + distanceX + ", scrollDeltaY: " + distanceY + ")");
            if (isRotate()) {
                rotate(distanceX, distanceY);
            } else {
                select(e1.getX(), e1.getY(), e2.getX(), e2.getY());
            }
            postInvalidateOnAnimation();
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, @NonNull MotionEvent e2, float velocityX, float velocityY) {
            // Log.d(TAG, "Called: onFling(velocityX: " + velocityX + ", velocityY: " + velocityY + ")");
            if (isRotate()) {
                final float distanceTimeFactor = 5.0f;
                totalDx = (distanceTimeFactor * velocityX / 2);
                totalDy = (distanceTimeFactor * velocityY / 2);
                xStep = totalDx / STEPS_PER_SPIN;
                yStep = totalDy / STEPS_PER_SPIN;
                postInvalidateOnAnimation();
            }
            return super.onFling(e1, e2, velocityX, velocityY);
        }
    };

    private final ScaleGestureDetector.SimpleOnScaleGestureListener scaleGestureListener = new ScaleGestureDetector.SimpleOnScaleGestureListener() {

        private float factor = 1.0f;

        @Override
        public boolean onScaleBegin(@NonNull ScaleGestureDetector detector) {
            return true;
        }

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            LogUtils.d(IS_RELEASE, TAG, "getScaleFactor(%s)", detector.getScaleFactor());
            if (detector.getScaleFactor() != 1.0f) {
                // -ve scale down, +ve scale up
                if (detector.getScaleFactor() < 1) {
                    factor = factor - 0.05f;
                } else {
                    factor = factor + 0.05f;
                }
                LogUtils.d(IS_RELEASE, TAG, "factor is: %s", factor);
                xfac = Math.abs(unitScale * factor);
                LogUtils.d(IS_RELEASE, TAG, "xfac is: %s", xfac);
            }
            return true;
        }
    };


    public ThreeDView(Context context) {
        super(context);
        init(context);
    }

    public ThreeDView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ThreeDView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @SuppressWarnings("unused")
    public ThreeDView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        gestureDetector = new GestureDetector(context, gestureListener);
        scaleGestureDetector = new ScaleGestureDetector(context, scaleGestureListener);

        selectionPaint.setColor(Color.CYAN);
        selectionPaint.setStrokeWidth(ViewUtils.convertDpSizeToRawPixels(context, 2));
        selectionPaint.setStyle(Paint.Style.STROKE);
        selectionPaint.setAntiAlias(true);
        resetPaint(context);

        selectionHandleSize = ViewUtils.convertDpSizeToRawPixels(context, 10.0f);
    }

    private void resetPaint(Context context) {
        paint.setColor(context.getColor(R.color.threeDLine));
        paint.setAntiAlias(true);
        paint.setStrokeWidth(ViewUtils.convertDpSizeToRawPixels(context, 1));
        paint.setAntiAlias(true);
    }

    public void setObject(Object3d object) {
        this.object = object;
        object.setSelectionHandleSize(selectionHandleSize);
        this.boundingBox = new BoundingBox(object.getVertices());
        postInvalidateOnAnimation();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        performClick();
        boolean retVal1 = gestureDetector.onTouchEvent(event);
        boolean retVal2 = scaleGestureDetector.onTouchEvent(event);
        return retVal1 || retVal2 || super.onTouchEvent(event);
    }

    @Override
    public boolean performClick() {
        return super.performClick();
    }

    @Override
    protected void onDraw(@NonNull Canvas canvas) {
        super.onDraw(canvas);
        if (selection != null && isSelect()) {
            canvas.drawRect(selection, selectionPaint);
        }
        if (object != null) {
            if (unitScale == 0.0f) {
                float xw = boundingBox.getWidth();
                float yw = boundingBox.getHeight();
                float zw = boundingBox.getDepth();
                if (yw > xw) {
                    xw = yw;
                }
                if (zw > xw) {
                    xw = zw;
                }
                float f1 = getWidth() / xw;
                float f2 = getHeight() / xw;
                // unitScale = 0.7f * (f1 < f2 ? f1 : f2);
                unitScale = 0.8f * (Math.min(f1, f2));
                xfac = unitScale;
            }
            // Log.d(TAG, "xfac is: " + xfac);
            matrix.unit();
            Point3D center = boundingBox.getCenter();
            // Move origin to center of object.
            matrix.translate(-center.getX(), -center.getY(), -center.getZ());
            // Apply and transforms to the matrix
            matrix.mult(transformMatrix);
            // Scale
            // matrix.scale(xfac, -xfac, 16 * xfac / getWidth());
            matrix.scale(xfac);

            // Move origin to center of screen.
            matrix.translate(getWidth() / 2f, getHeight() / 2f, 0);
            // Transform all verts using the resulting matrix
            object.transform(matrix);

            object.paint(canvas, paint);

            if (!isAlmostNothing(totalDx, 0.1f) && !isAlmostNothing(totalDy, 0.1f)) {
                rotate(totalDx / STEPS_PER_SPIN, totalDy / STEPS_PER_SPIN);
                totalDx = totalDx - xStep;
                totalDy = totalDy - yStep;
                invalidate();
            }
        }
    }

    private void rotate(float x, float y) {
        float xTheta = y * 360.0f / getWidth();
        float yTheta = x * 360.0f / getHeight();
        transformMatrix.xRotate(xTheta);
        transformMatrix.yRotate(yTheta);
    }

    private void select(float x1, float y1, float x2, float y2) {
        selection = new RectF();
        if (x1 < x2) {
            selection.left = x1;
            selection.right = x2;
        } else {
            selection.left = x2;
            selection.right = x1;
        }
        if (y1 < y2) {
            selection.top = y1;
            selection.bottom = y2;
        } else {
            selection.top = y2;
            selection.bottom = y1;
        }
        object.selectVertices(selection);
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public boolean isRotate() {
        return mode == MODE_ROTATE;
    }

    public boolean isSelect() {
        return mode == MODE_SELECT;
    }

    public void setDrawMode(Context context, int drawMode) {
        Log.d(TAG, "Called: setDrawMode(Context, " + drawMode + ")");
        if (drawMode == Object3d.DRAW_MODE_PLAIN || drawMode == Object3d.DRAW_MODE_GROUPS) {
            resetPaint(context);
        }
        if (object != null) {
            object.setDrawMode(drawMode);
            postInvalidateOnAnimation();
        }
    }

    public Object3d getObject() {
        return object;
    }

}
