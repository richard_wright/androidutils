package net.wrightnz.android.three;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.core.view.MenuCompat;

import net.wrightnz.android.three.geom.Object3d;
import net.wrightnz.android.three.view.ThreeDView;
import net.wrightnz.android.utils.Config;
import net.wrightnz.android.utils.ImageUtils;
import net.wrightnz.android.utils.LogUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Arrays;

@SuppressWarnings("TryFinallyCanBeTryWithResources")
public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String OBJECT_3D_KEY = "object3d";
    private static final int PERMISSIONS_READ_EXTERNAL_STORAGE = 2000;
    private static final String[] REQUIRED_PERMISSIONS = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static final CharSequence[] ITEMS = {"Take Photo", "Choose from Library"};

    private Config config;

    private ThreeDView threeDView;
    private ImageView imageView;

    private abstract class ImageCallBack implements ActivityResultCallback<ActivityResult> {

        protected void loadViews(Uri uri) {
            Log.i(TAG, "Called: loadViews(" + uri.getPath() + ")");
            try {
                Bitmap bmp = Object3DGenerator.findEdges(uri);
                imageView.setImageBitmap(bmp);
                Object3d model = Object3DGenerator.getObject3dFromImage(bmp);
                threeDView.setObject(model);
                Toast.makeText(MainActivity.this, "Imported image: " + uri, Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                Log.e(TAG, "Failed to import image.", e);
                Toast.makeText(MainActivity.this, "Failed to import image.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private final ActivityResultLauncher<Intent> loadFromCameraResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ImageCallBack() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Uri uri = ImageUtils.createImageUri(MainActivity.this, ImageUtils.PHOTO_FILE_NAME);
                        loadViews(uri);
                    }
                }
            });

    private final ActivityResultLauncher<Intent> loadFromUriResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ImageCallBack() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent in = result.getData();
                        if (result.getData() != null && in.getData() != null) {
                            Uri rawImageUri = ImageUtils.createImageUri(MainActivity.this, ImageUtils.PHOTO_FILE_NAME);
                            Uri uri = in.getData();
                            Log.d(TAG, "Intend uri: " + uri.getPath());
                            try {
                                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                                ImageUtils.saveBitmap(rawImageUri, bitmap);
                                loadViews(rawImageUri);
                            } catch (IOException e) {
                                Toast.makeText(MainActivity.this, "Failed to import image.", Toast.LENGTH_LONG).show();
                                Log.e(TAG, "Failed to import image.", e);
                            }
                        }
                    }
                }
            });

    private final ActivityResultLauncher<Intent> saveResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Log.i(TAG, "Received save file.");
                        Intent in = result.getData();
                        if (in != null && in.getData() != null) {
                            try {
                                OutputStream outputStream = getContentResolver().openOutputStream(in.getData());
                                if (outputStream != null) {
                                    try {
                                        Object3d model = threeDView.getObject();
                                        outputStream.write(model.toString().getBytes());
                                        Toast.makeText(MainActivity.this, "Object saved", Toast.LENGTH_SHORT).show();
                                    } finally {
                                        outputStream.flush();
                                        outputStream.close();
                                    }
                                }
                            } catch (IOException e) {
                                String message = String.format("Could not save object file: %s", e.getMessage());
                                Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                                Log.wtf(TAG, message, e);
                            }
                        }
                    }
                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.config = Config.getInstance(this, ResourceIds.RESOURCE_IDS);
        threeDView = findViewById(R.id.canvas);
        imageView = findViewById(R.id.preview);
        if (savedInstanceState != null) {
            Object3d object = savedInstanceState.getParcelable(OBJECT_3D_KEY);
            if (object != null) {
                threeDView.setObject(object);
            }
        } else {
            try {
                if (getIntent().getData() != null) {
                    long start = System.currentTimeMillis();
                    Uri file = getIntent().getData();
                    ContentResolver contentResolver = getContentResolver();
                    InputStream stream = contentResolver.openInputStream(file);
                    Object3d model = getObject3D(stream);
                    threeDView.setObject(model);
                    LogUtils.d(config.isProduction(), TAG, "Load time is: %s milliseconds", (System.currentTimeMillis() - start));
                } else {
                    threeDView.setObject(getObject3D(getAssets().open("ufo.obj")));
                }
            } catch (IOException e) {
                Log.wtf(TAG, "Failed to load Object", e);
            }
        }
    }

    private Object3d getObject3D(InputStream stream) throws IOException {
        OBJFileParser parser = new OBJFileParser();
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        try {
            parser.parse(reader);
            return parser.getObject();
        } finally {
            reader.close();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_menu, menu);
        MenuCompat.setGroupDividerEnabled(menu, true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_rotate) {
            item.setChecked(true);
            threeDView.setMode(ThreeDView.MODE_ROTATE);
            return false;
        } else if (id == R.id.action_select) {
            item.setChecked(true);
            threeDView.setMode(ThreeDView.MODE_SELECT);
            return false;
        } else if (id == R.id.action_show_materials) {
            item.setChecked(true);
            threeDView.setDrawMode(this, Object3d.DRAW_MODE_MATERIALS);
            return false;
        } else if (id == R.id.action_show_groups) {
            item.setChecked(true);
            threeDView.setDrawMode(this, Object3d.DRAW_MODE_GROUPS);
            return false;
        } else if (id == R.id.action_import) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                showImageSourceDialogue();
            } else {
                requestPermissions(REQUIRED_PERMISSIONS, PERMISSIONS_READ_EXTERNAL_STORAGE);
            }
        } else if (id == R.id.action_save) {
            try {
                save();
            } catch (Exception e) {
                Log.e(TAG, "Failed to same model", e);
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void save() throws IOException {
        LogUtils.i(config.isProduction(), TAG, "Called: save()");
        if (!isExternalStorageWritable()) {
            throw new IOException("External Storage is not writable.");
        }
        File file = new File(getExternalFilesDir(""), "NewObject.obj");
        Uri data = FileProvider.getUriForFile(this, "net.wrightnz.android.three.provider", file);

        Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setDataAndType(data, "text/plain");
        intent.putExtra(Intent.EXTRA_TITLE, "NewObject.obj");
        // Specify a URI for the directory that should be opened in the system file picker.
        intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, Uri.parse(DocumentsContract.EXTRA_INITIAL_URI));
        saveResultLauncher.launch(intent);
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_READ_EXTERNAL_STORAGE) {
            // If request is cancelled, the result arrays are empty.
            Log.i(TAG, "Permissions: " + Arrays.toString(permissions));
            Log.i(TAG, "grantResults: " + Arrays.toString(grantResults));
            if (permissions.length == 2) {
                Log.d(TAG, "READ_EXTERNAL_STORAGE && WRITE_EXTERNAL_STORAGE Granted :-)");
                showImageSourceDialogue();
            } else {
                Log.d(TAG, "READ_EXTERNAL_STORAGE && WRITE_EXTERNAL_STORAGE Denied :-(");
            }
        } else {
            Log.e(TAG, "onRequestPermissionsResult() received unsupported requestCode: " + requestCode);
        }
    }

    private void showImageSourceDialogue() {
        Log.i(TAG, "Called: showImageSourceDialogue()");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(ITEMS, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    startCameraActivity(new Intent(MediaStore.ACTION_IMAGE_CAPTURE));
                } else if (item == 1) {
                    loadFromUriResultLauncher.launch(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI));
                }
            }
        });
        builder.show();
    }

    private void startCameraActivity(Intent intent) {
        // Create the File where the photo should go.
        File photoFile = ImageUtils.createImageFile(this, ImageUtils.PHOTO_FILE_NAME);
        Uri photoUri = FileProvider.getUriForFile(this, "net.wrightnz.android.three.provider", photoFile);
        LogUtils.d(config.isProduction(), TAG, "photo Uri is: %s", photoUri);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        loadFromCameraResultLauncher.launch(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(OBJECT_3D_KEY, threeDView.getObject());
        super.onSaveInstanceState(outState);
    }

}
