package net.wrightnz.android.three;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import junit.framework.Assert;

import net.wrightnz.android.utils.LogUtils;
import net.wrightnz.android.utils.StringUtils;
import net.wrightnz.android.utils.ViewUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

import androidx.test.InstrumentationRegistry;
import androidx.test.filters.LargeTest;
import androidx.test.runner.AndroidJUnit4;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class BasicInstrumentedTest {

    private static final String TAG = BasicInstrumentedTest.class.getSimpleName();

    /*
    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(
            MainActivity.class);
     */

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("net.wrightnz.android.three", appContext.getPackageName());
        ViewUtils.setOverScrollColour(appContext, null, R.color.colorPrimary);

        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());

        onView(withText("Rotate"))     // withId(R.id.my_view) is a ViewMatcher
                .perform(click());     // click() is a ViewAction
        onView(withId(R.id.canvas)).perform(swipeLeft());
    }

    @Test
    public void testToTitleCase() {
        String result = StringUtils.toTitleCase("pink");
        Assert.assertEquals("Pink", result);
    }

    @Test
    public void testGetIntentAsString() throws Exception {
        Intent intent = new Intent("Action");
        intent.putExtra("key", "value");
        intent.putExtra("name", "Richard");
        Bundle bundle = new Bundle();
        bundle.putString("bundle_key_0", "bundle_value_0");
        bundle.putString("bundle_key_1", "bundle_value_1");
        intent.putExtra("bundle", bundle);
        String result = LogUtils.getIntentAsString(intent);
        Log.e("LogUtilsTest", result);
        org.junit.Assert.assertEquals("Intent(action=Action){Bundle:bundle{bundle_key_0=bundle_value_0 bundle_key_1=bundle_value_1 } key=value name=Richard }", result);
    }

}
