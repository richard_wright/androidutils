package net.wrightnz.android.three;

import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import net.wrightnz.android.three.geom.Object3d;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by Richard on 26/09/2017.
 */
@RunWith(AndroidJUnit4.class)
public class OBJFileParserTest {

    @Test
    public void parse() throws Exception {
        Context context = InstrumentationRegistry.getTargetContext();
        OBJFileParser parser = new OBJFileParser();
        parser.parse(new BufferedReader(new InputStreamReader(context.getAssets().open("MW.obj"))));
        Object3d result = parser.getObject();
        Assert.assertNotNull(result);
        Assert.assertEquals(572, result.getVertices().size());
    }

}