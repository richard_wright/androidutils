package net.wrightnz.android.utils.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created for (Spark NZ) on 6/12/17.
 *
 * @author Richard Wright
 */

public final class NetUtils {

    public static void checkConnection(Context context) throws IOException {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null) {
            throw new IOException("No Connectivity Manager");
        }
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork == null) {
            throw new IOException("No Network Info");
        }
        NetworkInfo.DetailedState state = activeNetwork.getDetailedState();
        if (!activeNetwork.isConnected()) {
            throw new IOException("Not connected to Network");
        }
    }

    public static Map<String, String> getDefaultHeaders(String referer, String userAgent) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json; charset=UTF-8");
        headers.put("Content-Type", "application/json; charset=UTF-8");
        headers.put("Referer", referer);
        headers.put("User-Agent", userAgent);
        headers.put("Cache-Control", "no-cache");
        return headers;
    }
}
