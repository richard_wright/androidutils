package net.wrightnz.android.utils.api;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.wrightnz.android.utils.LogUtils;
import net.wrightnz.android.utils.net.HttpResponseException;
import net.wrightnz.android.utils.net.IvyCookieManager;
import net.wrightnz.android.utils.net.NetUtils;
import net.wrightnz.android.utils.net.UnauthorisedException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.X509Certificate;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


/**
 * Created on 12/12/17.
 *
 * @author Richard Wright
 */
@SuppressWarnings("TryFinallyCanBeTryWithResources")
abstract class APIBase {

    private static final String TAG = APIBase.class.getSimpleName();

    protected Context context;
    private IvyCookieManager cookieManager;

    APIBase(Context context) {
        this.context = context;
        this.cookieManager = new IvyCookieManager();
    }

    protected void post(URL url) throws IOException, KeyManagementException, NoSuchAlgorithmException, NoSuchProviderException {
        this.post(url, null);
    }

    protected void post(URL url, Object object) throws IOException, KeyManagementException, NoSuchAlgorithmException, NoSuchProviderException {
        LogUtils.d(!isDebug(), TAG, "Called  post(%s, %s)", url, object);

        HttpURLConnection connection = getConnectionForPost(url);
        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
        try {
            if (object != null) {
                Gson gson = new GsonBuilder().create();
                String requestJson = gson.toJson(object);
                writer.write(requestJson);
            } else {
                writer.write("\n");
            }
            writer.flush();
            int responseCode = connection.getResponseCode();
            LogUtils.d(!isDebug(), TAG, cookieManager.getCookieValue());
            handleTerminalResponseCodes(connection, url, responseCode);
        } finally {
            writer.close();
        }
    }

    /**
     * @param connection
     * @param url          the url the request was sent to.
     * @param responseCode the http response code
     * @throws UnauthorisedException if the responseCode is a 401 or 403
     * @throws HttpResponseException if the responseCode is greater then 299
     */
    protected void handleTerminalResponseCodes(HttpURLConnection connection, URL url, int responseCode) throws IOException {
        String method = connection.getRequestMethod();
        String message = connection.getResponseMessage();
        if (responseCode == 401 || responseCode == 403) {
            throw new UnauthorisedException("Bigpipe API call " + url + " failed: " + message, responseCode, connection.getRequestMethod());
        }
        if (responseCode > 299) {
            throw new HttpResponseException("Bigpipe API call to " + url + " failed " + message, responseCode, method);
        }
    }

    protected void delete(URL url) throws IOException, KeyManagementException, NoSuchAlgorithmException, NoSuchProviderException {
        LogUtils.d(!isDebug(), TAG, "Called  truncateCustomer(%s)", url);
        HttpURLConnection connection = getConnectionForDelete(url);
        int responseCode = connection.getResponseCode();
        LogUtils.d(!isDebug(), TAG, cookieManager.getCookieValue());
        handleTerminalResponseCodes(connection, url, responseCode);
    }

    /**
     * @param url         the url to post to
     * @param object      the data to be posted
     * @param entityClass A class describing any response data
     * @param <T>         the type of the responseClass
     * @return an instance of the responseClass with any result sent back as a result of this post.
     */
    protected <T> T postWithResult(URL url, Object object, Class<T> entityClass) throws IOException, KeyManagementException, NoSuchAlgorithmException, NoSuchProviderException {
        LogUtils.d(!isDebug(), TAG, "Called  post(%s, %s, %s)", url, object, entityClass);
        Gson gson = new GsonBuilder().create();
        HttpURLConnection connection = getConnectionForPost(url);
        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
        try {
            if (object != null) {
                String requestJson = gson.toJson(object);
                LogUtils.d(!isDebug(), TAG, "RequestJson is %s", requestJson);
                writer.write(requestJson);
            } else {
                writer.write("\n");
            }
            writer.flush();
            int responseCode = connection.getResponseCode();
            LogUtils.d(!isDebug(), TAG, cookieManager.getCookieValue());
            T result = getResponseBody(connection, gson, entityClass);
            handleTerminalResponseCodes(connection, url, responseCode);
            return result;
        } finally {
            writer.close();
        }
    }

    protected GenericResponse postWithGenericResponse(URL url, Object object) throws IOException, KeyManagementException, NoSuchAlgorithmException, NoSuchProviderException {
        LogUtils.d(!isDebug(), TAG, "Called: post(%s, %s)", url, object);
        Gson gson = new GsonBuilder().create();
        HttpURLConnection connection = getConnectionForPost(url);
        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
        try {
            if (object != null) {
                String requestJson = gson.toJson(object);
                LogUtils.d(!isDebug(), TAG, "RequestJson is " + requestJson);
                writer.write(requestJson);
            } else {
                writer.write("\n");
            }
            writer.flush();
            int responseCode = connection.getResponseCode();
            if ((responseCode >= 200 && responseCode < 300) || responseCode == 500) {
                GenericResponse response = getResponseBody(connection, gson, GenericResponse.class);
                response.setResponseCode(responseCode);
                return response;
            } else {
                String method = connection.getRequestMethod();
                if (responseCode == 401 || responseCode == 403) {
                    throw new UnauthorisedException("Bigpipe API call to " + url + " failed", responseCode, method);
                } else {
                    throw new HttpResponseException("Bigpipe API call to " + url + " failed", responseCode, method);
                }
            }
        } finally {
            writer.close();
        }
    }


    /**
     * @param url           the url to post to
     * @param object        the data to be posted
     * @param responseClass A class describing any response data
     * @param <T>           the type of the responseClass
     * @return an instance of the responseClass with any result sent back as a result of this post.
     * @throws IOException
     * @throws KeyManagementException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    protected <T> T put(URL url, Object object, Class<T> responseClass) throws IOException, KeyManagementException, NoSuchAlgorithmException, NoSuchProviderException {
        LogUtils.d(!isDebug(), TAG, "Called  put(%s, %s, %s)", url, object, responseClass);
        Gson gson = new GsonBuilder().create();
        HttpURLConnection connection = getConnectionForPost(url);
        connection.setRequestMethod("PUT");
        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
        try {
            if (object != null) {
                String requestJson = gson.toJson(object);
                LogUtils.d(!isDebug(), TAG, "RequestJson is %s", requestJson);
                writer.write(requestJson);
            } else {
                writer.write("\n");
            }
            writer.flush();
            int responseCode = connection.getResponseCode();
            LogUtils.d(!isDebug(), TAG, cookieManager.getCookieValue());
            handleTerminalResponseCodes(connection, url, responseCode);
            if (responseClass != null) {
                return gson.fromJson(new InputStreamReader(connection.getInputStream()), responseClass);
            } else {
                return null;
            }
        } finally {
            writer.close();
        }
    }

    private <T> T getResponseBody(URLConnection connection, Gson gson, Class<T> entityClass) throws IOException {
        InputStream stream = connection.getInputStream();
        try {
            return gson.fromJson(new InputStreamReader(stream), entityClass);
        } finally {
            stream.close();
        }
    }

    protected <T> T get(URL url, Class<T> entityClass) throws IOException, KeyManagementException, NoSuchAlgorithmException, NoSuchProviderException {
        LogUtils.d(!isDebug(), TAG, "Called  getFirst(%s, %s)", url, entityClass);
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ").create();
        HttpURLConnection connection = getConnection(url);
        connection.setRequestMethod("GET");
        try {
            LogUtils.d(!isDebug(), TAG, cookieManager.getCookieValue());
            return getResponseBody(connection, gson, entityClass);
        } catch (Exception e) {
            int responseCode = connection.getResponseCode();
            String message = connection.getResponseMessage();
            if (responseCode == 401 || responseCode == 403) {
                throw new UnauthorisedException("Bigpipe API call " + url + " failed " + message, responseCode, "GET");
            } else {
                throw new HttpResponseException("Bigpipe API call " + url + " failed " + message, responseCode, "GET", e);
            }
        }
    }

    protected abstract boolean isDebug();

    protected HttpURLConnection getConnection(final URL url) throws IOException, KeyManagementException, NoSuchAlgorithmException, NoSuchProviderException {

        SSLContext sslContext = SSLContext.getInstance("SSL");
        if (isDebug()) {
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
        }

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        if (connection instanceof HttpsURLConnection) {
            HttpsURLConnection sslConnection = (HttpsURLConnection) connection;
            if (isDebug()) {
                sslConnection.setSSLSocketFactory(sslContext.getSocketFactory());
                sslConnection.setHostnameVerifier(allHostsValid);
            }
        }
        Map<String, String> defaultHeaders = NetUtils.getDefaultHeaders("", "");
        for (String key : defaultHeaders.keySet()) {
            connection.setRequestProperty(key, defaultHeaders.get(key));
        }
        return connection;
    }

    protected String getPlainText(final URL url) throws IOException, KeyManagementException, NoSuchAlgorithmException, NoSuchProviderException {
        LogUtils.d(!isDebug(), TAG, "Called  getPlainText(%s)", url);
        HttpURLConnection connection = getConnectionForPlainText(url);
        connection.setRequestMethod("GET");
        LogUtils.d(!isDebug(), TAG, cookieManager.getCookieValue());
        return getTextFromResponse(connection.getInputStream());
    }

    protected HttpURLConnection getConnectionForPlainText(final URL url) throws IOException, KeyManagementException, NoSuchAlgorithmException, NoSuchProviderException {
        HttpURLConnection connection = getConnection(url);
        connection.setRequestProperty("Accept", "text/plain; charset=UTF-8");
        connection.setRequestProperty("Content-Type", "text/plain; charset=UTF-8");
        return connection;
    }

    protected HttpURLConnection getConnectionForPost(final URL url) throws IOException, KeyManagementException, NoSuchAlgorithmException, NoSuchProviderException {
        HttpURLConnection connection = getConnection(url);
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        return connection;
    }

    protected HttpURLConnection getConnectionForDelete(final URL url) throws IOException, KeyManagementException, NoSuchAlgorithmException, NoSuchProviderException {
        HttpURLConnection connection = getConnection(url);
        connection.setRequestMethod("DELETE");
        return connection;
    }

    protected String getTextFromResponse(InputStream response) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(response));
        StringBuilder out = new StringBuilder();
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
            }
            return out.toString();
        } finally {
            reader.close();
        }
    }

    private TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
    };

    private HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

}
