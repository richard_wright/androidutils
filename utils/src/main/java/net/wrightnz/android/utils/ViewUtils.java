package net.wrightnz.android.utils;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EdgeEffect;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.lang.reflect.Field;

/**
 * Created by Richard Wright (Spark NZ Ventures) on 6/04/17.
 *
 * Various handy View utilities.
 */
@SuppressWarnings("WeakerAccess")
public final class ViewUtils {

    private static final String TAG = ViewUtils.class.getSimpleName();

    /*public static ObjectAnimator getButtonSpinnerAnimator(Context context, View view) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(view, "rotation", 0f, 360f);
        int spinnerDuration = context.getResources().getInteger(android.R.integer.config_mediumAnimTime);
        animator.setDuration(spinnerDuration);
        animator.setInterpolator(new LinearInterpolator());
        animator.setRepeatMode(ValueAnimator.RESTART);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        return animator;
    }*/

    public static void removeView(View view) {
        ViewGroup parent = getParent(view);
        if (parent != null) {
            parent.removeView(view);
        }
    }

    public static void replaceView(View currentView, View newView) {
        ViewGroup parent = getParent(currentView);
        if (parent == null) {
            return;
        }
        final int index = parent.indexOfChild(currentView);
        removeView(currentView);
        removeView(newView);
        parent.addView(newView, index);
    }

    public static ViewGroup getParent(View view) {
        return (ViewGroup) view.getParent();
    }


    /**
     * Colours all but the last layer in the specified LayerDrawable in a gradient with luminosity
     * decreasing by 10 for each layer
     *
     * @param titleBackground
     * @param colour          the starting colour (outer most layer) for the gradient. Ignored if isGrey is true.
     * @param strokeWidth     the stroke width to use when painting each layer
     * @param isGrey          start the gradent from white rather then the starting colour
     */
    public static void setDropShadowColour(LayerDrawable titleBackground, int colour, int strokeWidth, boolean isGrey) {
        if (isGrey) {
            colour = Color.WHITE;
        }
        for (int i = 0; i < titleBackground.getNumberOfLayers() - 1; i++) {
            GradientDrawable gradientDrawable = (GradientDrawable) titleBackground.getDrawable(i);
            gradientDrawable.setStroke(strokeWidth, colour);
            if (isGrey) {
                int red = Color.red(colour) - 10;
                colour = Color.argb(255, red, red, red);
            }
        }
    }


    public static ObjectAnimator getArgbAnimator(Context context, View viewToAnimate, int animatorResourceId) {
        ObjectAnimator animator = (ObjectAnimator) AnimatorInflater.loadAnimator(context, animatorResourceId);
        animator.setTarget(viewToAnimate);
        animator.setEvaluator(new ArgbEvaluator());
        return animator;
    }

    /**
     * @param scrollView   scroll view containing the view to expanded. This is required as expanding the
     *                     view might otherwise make the layout to big to fit the screen.
     * @param viewToExpand the view expand.
     * @param viewToFocus  view that will be scrolled into focus after viewToExpand is expanded.
     */
    public static void expandHeight(final ScrollView scrollView, final View viewToExpand, final View viewToFocus) {
        viewToExpand.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = viewToExpand.getMeasuredHeight();
        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        viewToExpand.getLayoutParams().height = 1;
        viewToExpand.setVisibility(View.VISIBLE);
        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    viewToExpand.getLayoutParams().height = RelativeLayout.LayoutParams.WRAP_CONTENT;
                } else {
                    viewToExpand.getLayoutParams().height = (int) (targetHeight * interpolatedTime);
                }
                viewToExpand.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                scrollView.requestChildFocus(viewToFocus, viewToFocus);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animation.setDuration(300);
        viewToExpand.startAnimation(animation);
    }

    public static void expandHeight(final ScrollView scrollView, final View view) {
        expandHeight(scrollView, view, view);
    }

    public static void expandWidth(final View view, Animation.AnimationListener listener) {
        view.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetWidth = view.getMeasuredWidth();
        view.getLayoutParams().width = 1;
        view.setVisibility(View.VISIBLE);
        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    view.getLayoutParams().width = RelativeLayout.LayoutParams.WRAP_CONTENT;
                } else {
                    view.getLayoutParams().width = (int) (targetWidth * interpolatedTime);
                }
                view.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        if (listener != null) {
            animation.setAnimationListener(listener);
        }
        // 1dp/ms
        animation.setDuration((int) (targetWidth / view.getContext().getResources().getDisplayMetrics().density));
        view.startAnimation(animation);
    }


    public static void collapseHeight(final View view) {
        final int initialHeight = view.getMeasuredHeight();

        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    view.setVisibility(View.GONE);
                } else {
                    view.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    view.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        // 1dp/ms
        animation.setDuration((int) (initialHeight / view.getContext().getResources().getDisplayMetrics().density));
        animation.setDuration(300);
        view.startAnimation(animation);
    }

    public static void showView(final View view, final int animTime) {
        view.setVisibility(View.VISIBLE);
        view.setScaleX(0.0F);
        view.setScaleY(0.0F);
        ViewPropertyAnimator animator = view.animate();
        //.rotationYBy(360)
        animator.setDuration(animTime).scaleX(1.0F).scaleY(1.0F).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.VISIBLE);
            }
        });
    }

    public static void goneView(final View view, final int animTime) {
        if (view.getVisibility() == View.VISIBLE) {
            view.setVisibility(View.VISIBLE);
            ViewPropertyAnimator animator = view.animate();
            animator.setDuration(animTime).alpha(0).scaleX(0.0F).scaleY(0.0F).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    view.setVisibility(View.GONE);
                }
            });
        }
    }

    public static void invisibleView(final View view, final int animTime) {
        if (view.getVisibility() == View.VISIBLE) {
            view.setVisibility(View.VISIBLE);
            ViewPropertyAnimator animator = view.animate();
            animator.setDuration(animTime).alpha(0).scaleX(0.0F).scaleY(0.0F).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    view.setVisibility(View.INVISIBLE);
                }
            });
        } else {
            view.setVisibility(View.INVISIBLE);
        }
    }

    public static void hideInputMethod(final Context context, final View view) {
        Log.d(TAG, "Called: hideInputMethod(Context, " + view + ")");
        if (view != null) {
            view.post(new Runnable() {
                @Override
                public void run() {
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            });
        }
    }

    public static void showInputMethod(final Context context,final View view) {
        Log.d(TAG, "Called: showInputMethod(Context, " + view + ")");
        if (view != null) {
            view.post(new Runnable() {
                @Override
                public void run() {
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
                }
            });
        }
    }

    public static int convertDpSizeToRawPixels(Context context, float dpSize) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float scale = displayMetrics.density;
        return (int)(dpSize * scale + 0.5f);
    }

    public static float convertPixelsToDp(Context context, int pixels) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(pixels / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static void setTextOrGone(TextView view, CharSequence text) {
        if (TextUtils.isEmpty(text)) {
            view.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
            view.setText(text);
        }
    }

    public static void setOverScrollColour(Context context, ScrollView view, int colourId) {
        int colour = ContextCompat.getColor(context, colourId);
        EdgeEffect edgeEffectTop = new EdgeEffect(context);
        edgeEffectTop.setColor(colour);
        EdgeEffect edgeEffectBottom = new EdgeEffect(context);
        edgeEffectBottom.setColor(colour);
        try {
            Field f1 = ScrollView.class.getDeclaredField("mEdgeGlowTop");
            f1.setAccessible(true);
            f1.set(view, edgeEffectTop);

            Field f2 = ScrollView.class.getDeclaredField("mEdgeGlowBottom");
            f2.setAccessible(true);
            f2.set(view, edgeEffectBottom);
        } catch (Exception e) {
            Log.wtf(TAG, "Failed to set Scroll Colour for view: " + view, e);
        }
    }

}
