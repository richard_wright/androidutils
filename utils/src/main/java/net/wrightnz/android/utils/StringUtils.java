package net.wrightnz.android.utils;

/**
 * Created by Richard Wright (Spark NZ Ventures) on 18/01/17.
 *
 * Home for String manipulation stuff.
 */
public final class StringUtils {

    /**
     * @return the input String with the first letter of each word in upper case.
     * The rest of the characters in each word being converted to lower case.
     */
    public static String toTitleCase(final String string) {
        StringBuilder result = new StringBuilder();
        String[] words = string.toLowerCase().split(" ");
        for (int i = 0; i < words.length; i++) {
            String token = words[i];
            if (!token.isEmpty()) {
                Character firstChar = token.charAt(0);
                String firstLetter = firstChar.toString().toUpperCase();
                result.append(firstLetter);
                result.append(token.substring(1));
                if (i < words.length - 1) {
                    result.append(" ");
                }
            }
        }
        return result.toString();
    }

}
