package net.wrightnz.android.utils.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import net.wrightnz.android.utils.net.APICall;
import net.wrightnz.android.utils.net.HttpResponseException;
import net.wrightnz.android.utils.net.NetUtils;
import net.wrightnz.android.utils.net.RequestPartlyCompletedException;
import net.wrightnz.android.utils.net.ServiceResponseActions;
import net.wrightnz.android.utils.net.ServiceResponseCodes;
import net.wrightnz.android.utils.net.UnauthorisedException;

/**
 * Created for me on 6/12/17.
 *
 * @author Richard Wright
 */
public abstract class ApiCallIntentService extends IntentService {

    private static final String TAG = ApiCallIntentService.class.getSimpleName();

    public ApiCallIntentService(String name) {
        super(name);
    }

    /**
     * Calls the specified APICall subclass and retries in the call fails due to the currently
     * stored session cookie no longer being valid.
     *
     * @param call the APICall implementation being made.
     * @param <T> type that this service returns from the Bippipe APIs.
     * @return response of type T
     * @throws Exception if anything goes wrong caller should check for FileNotFoundException which
     *                   indicates the call was not authorised even after a new session token was refreshed, most likely
     *                   the result of password expiry of reset.
     */
    protected <T> T makeAPICall(APICall<T> call) throws Exception {
        NetUtils.checkConnection(this.getApplicationContext());
        try {
            return call.call();
        } catch (UnauthorisedException e) {
            try {
                return call.call();
            } catch (UnauthorisedException ex) {
                Log.i(TAG, "Users credentials might have been changed.");
                // Broadcast the message that user's credentials might have been changed
                Intent response = new Intent(ServiceResponseActions.ACTION_ON_AUTHENTICATION_FAILURE);
                // LocalBroadcastManager.getInstance(this).sendBroadcast(response);
                throw ex;
            }
        }
    }

    protected String getResponseCodeForException(Exception e) {
        if (e instanceof UnauthorisedException) {
            return ServiceResponseCodes.AUTHENTICATION_ERROR;
        } else if (e instanceof RequestPartlyCompletedException) {
            return ServiceResponseCodes.PARTLY_SUCCESSFUL;
        } else if (e instanceof HttpResponseException) {
            HttpResponseException responseException = (HttpResponseException) e;
            if (responseException.getHttpResponseCode() == 405
                    || responseException.getHttpResponseCode() == 404
                    || responseException.getHttpResponseCode() == 500) {
                return ServiceResponseCodes.FAILED;
            } else {
                return ServiceResponseCodes.NETWORK_ERROR;
            }
        } else {
            return ServiceResponseCodes.NETWORK_ERROR;
        }
    }

}
