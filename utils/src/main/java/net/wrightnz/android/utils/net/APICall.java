package net.wrightnz.android.utils.net;

/**
 * Created by Richard Wright on 5/04/16.
 *
 * Represents a call (or collection of calls) to the Bigpipe APIs.
 * The parameter is the return type of the API call.
 *
 * e.g. for a simple call the the Me API we would have:
 *
 * class MeAPICall implements APICall&lt;Me&gt;{
 *     ...
 * }
 *
 * This is a functional interface used to wrap API calls used in BigpipeAPIIntentService
 * which abstracts away the nastiness of retrying API calls when session cookies expire.
 */
@FunctionalInterface
public interface APICall<T> {

    T call() throws Exception;

}
