package net.wrightnz.android.utils.net;

/**
 * Created by Richard Wright on 5/04/16.
 */
public final class ServiceResponseCodes {

    public static final String OK = "OK";
    public static final String FAILED = "FAILED";
    public static final String PARTLY_SUCCESSFUL = "PARTLY_SUCCESSFUL";
    public static final String AUTHENTICATION_ERROR = "AUTHENTICATION_ERROR";
    public static final String NETWORK_ERROR = "NETWORK_ERROR";

}
