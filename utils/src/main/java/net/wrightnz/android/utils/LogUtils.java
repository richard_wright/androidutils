package net.wrightnz.android.utils;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

/**
 * Created on 18/01/17.
 *
 * @author Richard Wright
 *
 * Useful utility methods for logging.
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public final class LogUtils {

    public static void i(boolean isProduction, String tag, String message, Object... args) {
        i(isProduction, tag, String.format(message, args));
    }

    public static void i(boolean isProduction, String tag, String message) {
        if (!isProduction) {
            Log.i(tag, message);
        }
    }

    public static void d(boolean isProduction, String tag, String message, Object... args) {
        d(isProduction, tag, String.format(message, args));
    }

    public static void d(boolean isProduction, String tag, String message) {
        if (!isProduction) {
            Log.d(tag, message);
        }
    }

    public static void logIntent(boolean isProduction, String tag, Intent intent) {
        if (!isProduction) {
            Log.d(tag, getIntentAsString(intent));
        }
    }

    public static void printBundle(boolean isProduction, String tag, Bundle bundle) {
        if (!isProduction) {
            StringBuilder sb = new StringBuilder();
            writeBundle(sb, bundle);
            Log.d(tag, sb.toString());
        }
    }

    public static String getIntentAsString(Intent intent) {
        StringBuilder sb = new StringBuilder();
        writeIntent(sb, intent);
        return sb.toString();
    }

    private static void writeIntent(StringBuilder sb, Intent intent) {
        sb.append("Intent(action=");
        sb.append(intent.getAction());
        sb.append("){");
        writeBundle(sb, intent.getExtras());
        sb.append("}");
    }

    private static void writeBundle(StringBuilder sb, Bundle bundle) {
        if (bundle != null && bundle.keySet() != null) {
            for (String key : bundle.keySet()) {
                Object value = bundle.get(key);
                if (value instanceof Bundle) {
                    sb.append("Bundle:");
                    sb.append(key);
                    sb.append("{");
                    writeBundle(sb, (Bundle) value);
                    sb.append("} ");
                } else if (value instanceof Intent) {
                    writeIntent(sb, (Intent) value);
                } else {
                    sb.append(key);
                    sb.append("=");
                    sb.append(value);
                    sb.append(" ");
                }
            }
        } else {
            sb.append("Empty");
        }
    }

}
