package net.wrightnz.android.utils.net;

/**
 * Created on 29/11/17.
 * A container for all the possable response actions the Bigpipe Apps Intent
 * Service can broadcast.
 *
 * @author Richard Wright
 */
public final class ServiceResponseActions {

    public static final String ACTION_ON_AUTHENTICATION_FAILURE = "nz.co.bigpipe.authentication.failed";
    public static final String ACTION_ON_CACHE_UPDATED = "nz.co.bigpipe.cache.updated";
    public static final String ACTION_ON_CHANGE_REQUESTED = "nz.co.bigpipe.change.requested";

}
