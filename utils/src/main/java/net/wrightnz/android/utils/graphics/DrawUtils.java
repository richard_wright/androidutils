package net.wrightnz.android.utils.graphics;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by Richard Wright (Spark NZ Ventures) on 29/05/17.
 *
 * @author Richard Wright
 * <p>
 * Place for abstractions of android.graphics.Canvas drawing operations
 */
public final class DrawUtils {

    public static void fillOval(Canvas canvas, int x, int y, int width, int height, Paint paint) {
        ;
        canvas.drawOval(x, y, x + width, y + height, paint);
    }

    public static void fillPolygon(Canvas canvas, int[] xis, int[] yis, int verts, Paint paint) {
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        drawPolygon(canvas, xis, yis, verts, paint);
        paint.setStyle(Paint.Style.STROKE);
    }

    public static void drawPolygon(Canvas canvas, int[] xis, int[] yis, int verts, Paint paint) {
        for (int i = 0; i < verts - 1; i++) {
            canvas.drawLine(xis[i], yis[i], xis[i + 1], yis[i + 1], paint);
        }
        // Close the polygon
        if (verts > 0) {
            canvas.drawLine(xis[0], yis[0], xis[verts - 1], yis[verts - 1], paint);
        }
    }
}
