package net.wrightnz.android.utils.api;

/**
 * Created by Richard Wright on 10/11/16.
 *
 * POJO for the generic success/failure responses.
 */
public class GenericResponse {

    private String status;
    private String error;
    private int responseCode;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(GenericResponse.class.getSimpleName());
        sb.append("{");
        sb.append("status: ").append(status);
        sb.append(", error: ").append(error);
        sb.append(", responseCode: ").append(responseCode);
        sb.append("}");
        return sb.toString();
    }
}
