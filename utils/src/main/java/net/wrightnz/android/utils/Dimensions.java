package net.wrightnz.android.utils;

/**
 * Created by Richard Wright (Spark NZ Ventures) on 10/03/17.
 *
 * Wrapper for image width and height.
 *
 * @author Richard Wright
 */
public class Dimensions {

    private int width;
    private int height;

    public Dimensions() {
    }

    public Dimensions(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(Dimensions.class.getSimpleName());
        sb.append("{");
        sb.append("width: ").append(width);
        sb.append(", height: ").append(height);
        sb.append("}");
        return sb.toString();
    }
}
