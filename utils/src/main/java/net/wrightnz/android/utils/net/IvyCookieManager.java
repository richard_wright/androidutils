package net.wrightnz.android.utils.net;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.util.List;

/**
 * Created by Richard Wright on 14/11/17.
 *
 * @author Richard Wright.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class IvyCookieManager {

    private CookieManager cookieManager;

    public IvyCookieManager() {
        cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(cookieManager);
    }

    List<HttpCookie> getCookies() {
        return cookieManager.getCookieStore().getCookies();
    }

    public void clearCookies() {
        cookieManager.getCookieStore().removeAll();
    }

    public boolean isCookieManagerEmpty() {
        return cookieManager.getCookieStore().getCookies().isEmpty();
    }

    public String getCookieValue() {
        StringBuilder cookieValue = new StringBuilder();
        for (HttpCookie cookie : getCookies()) {
            cookieValue.append(String.format("%s=%s; ", cookie.getName(), cookie.getValue()));
        }
        return cookieValue.toString();
    }

}
