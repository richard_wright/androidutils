package net.wrightnz.android.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;

import androidx.exifinterface.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import androidx.annotation.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static androidx.annotation.RestrictTo.Scope.LIBRARY_GROUP;

/**
 * Created by Richard Wright (Spark NZ Ventures) on 16/06/17.
 * @noinspection TryFinallyCanBeTryWithResources
 */
@SuppressWarnings("WeakerAccess")
public final class ImageUtils {

    @RestrictTo(LIBRARY_GROUP)
    @StringDef({PHOTO_FILE_NAME, EDGE_FILE_NAME})
    @Retention(RetentionPolicy.SOURCE)
    public @interface FileName {}

    public static final String PHOTO_FILE_NAME = "three_import_image.jpg";
    public static final String EDGE_FILE_NAME = "three_edge_image.jpg";

    private ImageUtils() {
    }

    public static File createImageFile(Context context, @FileName String filename) {
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return new File(storageDir, filename);
    }

    public static Uri createImageUri(Context context, String filename) {
        return Uri.fromFile(createImageFile(context, filename));
    }

    public static void saveBitmap(Uri uri, Bitmap bitmap) throws IOException {
        FileOutputStream out = new FileOutputStream(uri.getPath());
        try {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
        } finally {
            out.close();
        }
    }

    public static int[] getPixels(Bitmap bitmap) {
        int[] pixels = new int[bitmap.getWidth() * bitmap.getHeight()];
        bitmap.getPixels(pixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        return pixels;
    }

    public static Bitmap loadImage(Uri uri) throws IOException {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(uri.getPath(), options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, 1080, 1080);
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        // Decode bitmap with inSampleSize set
        Bitmap image = BitmapFactory.decodeFile(uri.getPath(), options);
        return fixOrientation(new File(uri.getPath()), image);
    }

    public static Bitmap createBitmap(int[] pixels, int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    /**
     * Calculate the largest inSampleSize value that is a power of 2 and keeps both
     * height and width larger than the requested height and width.
     *
     * @param options   bitnap options for outWidth and outHeight
     * @param reqWidth  requested width
     * @param reqHeight requested height
     * @return the largest inSampleSize value that is a power of 2 and keeps both
     * height and width larger than the requested height and width.
     */
    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) >= reqHeight && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }


    public static Bitmap rescaleImage(Bitmap bitmap, int maxWidth, int maxHeight) {
        Dimensions widthHeight = rescale(new Dimensions(bitmap.getWidth(), bitmap.getHeight()), maxWidth, maxHeight);
        return Bitmap.createScaledBitmap(bitmap, widthHeight.getWidth(), widthHeight.getHeight(), false);
    }

    static Dimensions rescale(Dimensions source, int maxWidth, int maxHeight) {
        Dimensions result = new Dimensions(source.getWidth(), source.getHeight());
        if (source.getWidth() < maxWidth && source.getHeight() < maxHeight) {
            result.setWidth(maxWidth);
            result.setHeight(maxHeight);
            if (source.getWidth() > source.getHeight()) {
                float ratio = (float) maxWidth / (float) source.getWidth();
                int newHeight = (int) (ratio * source.getHeight());
                result.setHeight(newHeight);
            } else if (source.getWidth() < source.getHeight()) {
                float ratio = (float) maxHeight / (float) source.getHeight();
                int newWidth = (int) (ratio * source.getWidth());
                result.setWidth(newWidth);
            }
            return result;
        }
        if (source.getWidth() > maxWidth) {
            float scale = getScale(source.getWidth(), maxWidth);
            result.setWidth((int) (source.getWidth() * scale));
            result.setHeight((int) (source.getHeight() * scale));
        }
        if (source.getHeight() > maxHeight) {
            float scale = getScale(source.getHeight(), maxHeight);
            result.setHeight((int) (source.getHeight() * scale));
            result.setWidth((int) (source.getWidth() * scale));
        }
        if (result.getWidth() > maxWidth) {
            float scale = getScale(result.getWidth(), maxWidth);
            result.setWidth((int) (result.getWidth() * scale));
            result.setHeight((int) (result.getHeight() * scale));
        }
        if (result.getHeight() > maxHeight) {
            float scale = getScale(result.getHeight(), maxHeight);
            result.setHeight((int) (result.getHeight() * scale));
            result.setWidth((int) (result.getWidth() * scale));
        }
        return result;
    }

    static float getScale(int size, int maxSize) {
        if (size > maxSize) {
            return ((float) maxSize) / ((float) size);
        } else {
            return 1.0f;
        }
    }

    private static Bitmap fixOrientation(File file, Bitmap bitmap) throws IOException {
        ExifInterface ei = new ExifInterface(file.getAbsolutePath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(bitmap, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(bitmap, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(bitmap, 270);
            default:
                return bitmap;
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    /**
     * Keeps int in range 0 to 255.
     */
    public static int colourLimiter(int val) {
        if (val < 0) {
            return 0;
        }
        return Math.min(val, 255);
    }

    /**
     * Check if to colours are within dif of each other.
     */
    public static boolean sameColour(int col1, int col2, int dif) {
        int rDif = Math.abs(Color.red(col1) - Color.red(col2));
        int gDif = Math.abs(Color.green(col1) - Color.green(col2));
        int bDif = Math.abs(Color.blue(col1) - Color.blue(col2));
        return rDif <= dif && gDif <= dif && bDif <= dif;
    }

    /**
     * See if difference between two colour values is big
     * enough (that is greater then 5)
     * and if so exaggerate it, otherwise blacken.
     * Used for edge detector.
     */
    public static int[] edgeFind(int col1, int col2) {
        int[] colours = new int[2];
        int dif = Math.abs(col1 - col2);

        if (col1 > col2 && dif > 5) {
            colours[0] = colourLimiter(col1 + 150);
            colours[1] = colourLimiter(col2 - 150);
        } else if (col1 < col2 && dif > 5) {
            colours[0] = colourLimiter(col1 - 150);
            colours[1] = colourLimiter(col2 + 150);
        }
        return colours;
    }

    /**
     * See if difference between to colour values is big enough
     * and if so exaggerate it.
     */
    public static int[] edgeEnhance(int col1, int col2) {
        int[] colours = new int[2];
        int dif = Math.abs(col1 - col2);

        if (col1 > col2 && dif > 10) {
            colours[0] = colourLimiter(col1 + 50);
            colours[1] = col2;
        } else if (col1 < col2 && dif > 10) {
            colours[0] = col1;
            colours[1] = colourLimiter(col2 + 50);
        } else {
            colours[0] = col1;
            colours[1] = col2;
        }
        return colours;
    }


    public static int[] edgeFinder(int pixel1, int pixel2, int pixel3) {
        int[] colours = new int[3];
        int lum1 = Color.blue(pixel1);
        int lum2 = Color.blue(pixel2);
        int lum3 = Color.blue(pixel3);

        int difference1 = Math.abs(lum1 - lum2);
        int difference2 = Math.abs(lum2 - lum3);

        if (difference1 > 5) {
            if (lum1 < lum2) {
                colours[0] = Color.BLACK;
                colours[1] = Color.WHITE;
            } else {
                colours[0] = Color.WHITE;
                colours[1] = Color.BLACK;
            }
            colours[2] = Color.BLACK;
        } else if (difference2 > 5) {
            colours[0] = Color.BLACK;
            if (lum2 < lum3) {
                colours[1] = Color.BLACK;
                colours[2] = Color.WHITE;
            } else {
                colours[1] = Color.WHITE;
                colours[2] = Color.BLACK;
            }
        } else {
            colours[0] = Color.BLACK;
            colours[1] = Color.BLACK;
            colours[2] = Color.BLACK;
        }
        return colours;
    }

    /**
     * Convert 2D array to 1D array
     */
    public static int[] two2One(int[][] array2D) {
        int width = array2D.length;
        int height = array2D[0].length;
        int[] array1D = new int[width * height];
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                array1D[y * width + x] = array2D[x][y];
            }
        }
        return array1D;
    }

    /**
     * Convert 1D array to 2D array
     */
    public static int[][] one2Two(int[] array, int width, int height) {
        int[][] result = new int[height][width];
        for (int i = 0; i < height; i++) {
            System.arraycopy(array, (i * width), result[i], 0, width);
        }
        return result;
    }

}
