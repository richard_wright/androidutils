package net.wrightnz.android.utils.net;

import java.io.IOException;

/**
 * Created by Richard Wright on 15/09/16.
 */
public class HttpResponseException extends IOException {

    private String method;
    private int httpResponseCode;

    public HttpResponseException(String message, int responseCode, String method) {
        super(message);
        this.httpResponseCode = responseCode;
        this.method = method;
    }

    public HttpResponseException(String message, int responseCode, String method, Throwable cause) {
        super(message, cause);
        this.httpResponseCode = responseCode;
        this.method = method;
    }

    public int getHttpResponseCode() {
        return httpResponseCode;
    }

    public String getMethod() {
        return method;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append("[");
        sb.append(getMessage());
        sb.append(", httpResponseCode: ");
        sb.append(httpResponseCode);
        if (method != null) {
            sb.append(", method: ");
            sb.append(method);
        }
        sb.append("]");
        if (getCause() != null) {
            sb.append("\n");
            sb.append("Caused By:\n");
            sb.append(getCause());
        }
        return sb.toString();
    }

}
