package net.wrightnz.android.utils.net;

/**
 * Created by Richard Wright on 10/11/16.
 *
 * Specific exception to signal that a restful service call was only partly successful.
 * i.e. the service action was not atomic and some but not all of the action was successful.
 */
public class RequestPartlyCompletedException extends HttpResponseException {

    public RequestPartlyCompletedException(String message, int httpResponseCode, String method){
        super(message, httpResponseCode, method);
    }

}
