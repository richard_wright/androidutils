package net.wrightnz.android.utils.net;

/**
 * Created by Richard Wright on 26/07/16.
 */
public class UnauthorisedException extends HttpResponseException {

    public UnauthorisedException(String message, int httpResponseCode, String method){
        super(message, httpResponseCode, method);
    }

}
