package net.wrightnz.android.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

/**
 * Created by Richard Wright (Spark NZ Ventures) on 17/02/16.
 * <p>
 * Basically just wraps the Android context in order to give the app a more convenient way
 * to access various properties and resources.
 *
 * @author Richard Wright
 */
@SuppressWarnings("SimplifiableIfStatement")
public class Config {

    private static final String TAG = Config.class.getSimpleName();
    private static Config instance;

    private boolean isProduction;
    private String apiDomain;
    private String protocol;
    private String userAgent;
    private int connectionTimeOut;
    private int readTimeOut;
    private String appVersion;
    private int appVersionCode;
    private PackageInfo packageInfo;

    /*
    {R.string.api_protocol,
    R.bool.is_production,
    R.string.user_agent,
    R.integer.connection_timeout_mills,
    R.integer.read_timeout_mills,
    R.string.api_domain}
     */
    private Config(Context context, int[] resourceIds) {
        protocol = context.getString(resourceIds[0]);
        isProduction = context.getResources().getBoolean(resourceIds[1]);
        appVersion = "";
        appVersionCode = 0;
        userAgent = context.getString(resourceIds[2]);
        if (initPackageInfo(context)) {
            appVersion = packageInfo.versionName;
            appVersionCode = packageInfo.versionCode;
            userAgent =  userAgent + " " + this.appVersion;
        }
        connectionTimeOut = context.getResources().getInteger(resourceIds[3]);
        readTimeOut = context.getResources().getInteger(resourceIds[4]);
        apiDomain = context.getString(resourceIds[5]);
    }

    public static Config getInstance(Context context, int[] resourceIds) {
        if (instance == null) {
            instance = new Config(context, resourceIds);
        }
        return instance;
    }

    /**
     * Initialize the packageInfo field. packageInfo field is used to fetch application
     * variables defined in the defaultConfig.
     *
     * @param context Android Application Context
     * @return true if initialization succeed. Otherwise return false.
     */
    private boolean initPackageInfo(Context context) {
        try {
            String packageName = context.getPackageName();
            this.packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            Log.wtf("ViewUtils", "Could not lookup package name", e);
            return false;
        }
    }

    public boolean isProduction() {
        return isProduction;
    }

    public void setProduction(boolean production) {
        isProduction = production;
    }

    public String getApiDomain() {
        return apiDomain;
    }

    public void setApiDomain(String apiDomain) {
        this.apiDomain = apiDomain;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public int getConnectionTimeOut() {
        return connectionTimeOut;
    }

    public void setConnectionTimeOut(int connectionTimeOut) {
        this.connectionTimeOut = connectionTimeOut;
    }

    public String getAppVersion() {
        return this.appVersion;
    }

    public int getReadTimeOut() {
        return readTimeOut;
    }

    public void setReadTimeOut(int readTimeOut) {
        this.readTimeOut = readTimeOut;
    }

    public String getProtocol() {
        return protocol;
    }

    public int getAppVersionCode() {
        return appVersionCode;
    }

    public String getApiBaseUrl() {
        return protocol + "://" + apiDomain;
    }

}
