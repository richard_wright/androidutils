package net.wrightnz.android.utils;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
// import android.support.v4.content.LocalBroadcastManager;

import net.wrightnz.android.utils.net.ServiceResponseCodes;

/**
 * Created for on 30/11/17.
 *
 * @author Richard Wright
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class BroadcastUtils {

    private static final String RESPONSE_CODE_KEY = "net.wrightnz.broadcast.response.code";
    private static final String RESPONSE_DATA_KEY = "net.wrightnz.broadcast.response.data";

    public static Intent buildResponseIntent(String action, String responseCode, Parcelable responseData) {
        Intent intent = new Intent(action);
        intent.putExtra(RESPONSE_CODE_KEY, responseCode);
        if(responseData != null) {
            intent.putExtra(RESPONSE_DATA_KEY, responseData);
        }
        return intent;
    }

    public static Intent buildResponseIntent(String action, String responseCode) {
        Intent intent = new Intent(action);
        intent.putExtra(RESPONSE_CODE_KEY, responseCode);
        return intent;
    }

    /**
     * Broadcast and Intent with the specified action and containing the
     * specified responseCode and responseData
     *
     */
    public static void sendBroadcast(Context context,String action, String responseCode, Parcelable responseData) {
        Intent response = buildResponseIntent(action, responseCode, responseData);
        // LocalBroadcastManager.getInstance(context).sendBroadcast(response);
    }

    public static <T extends Parcelable> T getResponseData(Intent response) {
        return response.getParcelableExtra(RESPONSE_DATA_KEY);
    }

    public static String getResponseString( Intent response) {
        return response.getStringExtra(RESPONSE_DATA_KEY);
    }

    /**
     * Test if the broadcast response intent has the specified response code.
     *
     * @param intent       the broadcast response intent
     * @param responseCode the response code to test for.
     * @return true if and only if the broadcast response intent contains the specified
     * response code.
     */
    public static boolean hasResponseCode(Intent intent, String responseCode) {
        String code = intent.getStringExtra(RESPONSE_CODE_KEY);
        return responseCode.equals(code);
    }

    public static boolean isOkResponse(Intent intent){
        return hasResponseCode(intent, ServiceResponseCodes.OK);
    }

}
