package net.wrightnz.android.utils;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void testArrayOne2Two() throws Exception {
        int[] input = {1, 2, 3, 4, 5, 6};
        int[][] expected = {{1, 2, 3}, {4, 5, 6}};
        int[][] result = ImageUtils.one2Two(input, 3, 2);

        Assert.assertEquals(2, result.length);
        Assert.assertEquals(3, result[0].length);
        Assert.assertEquals(5, result[1][1]);
        Assert.assertEquals(4, result[1][0]);
        Assert.assertArrayEquals(expected, result);
    }

}
