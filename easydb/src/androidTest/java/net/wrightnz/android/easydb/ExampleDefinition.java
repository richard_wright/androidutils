package net.wrightnz.android.easydb;

import java.util.Collections;
import java.util.List;

/**
 * Created for (Spark NZ) on 19/12/17.
 *
 * @author Richard Wright
 */

public class ExampleDefinition implements DatabaseDefinition {
    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Example.db";

    private List<ExampleTable> tables = Collections.singletonList(new ExampleTable());

    @Override
    public String getName() {
        return DATABASE_NAME;
    }

    @Override
    public int getVersion() {
        return DATABASE_VERSION;
    }

    @Override
    public List<? extends Table> getTables() {
        return tables;
    }
}
