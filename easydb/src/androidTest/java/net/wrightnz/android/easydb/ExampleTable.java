package net.wrightnz.android.easydb;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;

/**
 * Created by Richard Wright on 6/04/17.
 *
 * @author Richard Wright
 */
public final class ExampleTable extends Table<Bundle> {

    private static final Column[] COLUMNS = {
            COLUMN_ID,
            new Column("title", "TEXT"),
            new Column("message", "TEXT"),
            new Column("last_modified", "TEXT")
    };

    @Override
    public Column[] getColumns() {
        return COLUMNS;
    }

    /**
     * Create a new map of values, where column names are the keys
     */
    @Override
    public ContentValues toContentValues(Bundle bundle) {
        ContentValues values = new ContentValues();
        values.put(COLUMNS[0].getName(), bundle.getInt(COLUMNS[0].getName()));
        values.put(COLUMNS[1].getName(), bundle.getString(COLUMNS[1].getName()));
        values.put(COLUMNS[2].getName(), bundle.getString(COLUMNS[2].getName()));
        values.put(COLUMNS[3].getName(), bundle.getString(COLUMNS[3].getName()));
        return values;
    }

    @Override
    public Bundle fromCursor(Cursor cursor) {
        Bundle bundle = new Bundle();
        bundle.putString(COLUMNS[0].getName(), String.valueOf(cursor.getInt(0)));
        bundle.putString(COLUMNS[1].getName(), cursor.getString(1));
        bundle.putString(COLUMNS[2].getName(), cursor.getString(2));
        bundle.putString(COLUMNS[3].getName(), cursor.getString(3));
        return bundle;
    }

}
