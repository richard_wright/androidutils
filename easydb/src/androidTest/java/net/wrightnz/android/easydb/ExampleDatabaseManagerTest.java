package net.wrightnz.android.easydb;

import android.content.Context;
import android.os.Bundle;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

/**
 * Created for (Spark NZ) on 19/12/17.
 *
 * @author Richard Wright
 *
 */
@RunWith(AndroidJUnit4.class)
public class ExampleDatabaseManagerTest {

    private static final String TAG = ExampleDatabaseManagerTest.class.getSimpleName();

    @Test
    public void testInsertSelectUpdateDelete() throws Exception {
        // The context of the app under test.
        Context context = InstrumentationRegistry.getTargetContext();

        DatabaseManager manager = DatabaseManager.getInstance(context, new ExampleDefinition());
        Table<Bundle> table = new ExampleTable();

        manager.deleteAll(table);
        Bundle bundle = new Bundle();
        bundle.putInt("_id", 0);
        bundle.putString("title", "Just Testing");
        bundle.putString("message", "Message");
        bundle.putString("last_modified", "2017-12-19");
        manager.insert(table, bundle);

        Bundle read = manager.select(table, 0);
        Assert.assertNotNull(read);

        Assert.assertEquals(0, read.getInt("_id"));
        Assert.assertEquals("Just Testing", read.getString("title"));
        Assert.assertEquals("Message", read.getString("message"));
        Assert.assertEquals("2017-12-19", read.getString("last_modified"));

        read.putString("message", "New Message");

        manager.update(table, read, 0);

        List<Bundle> result2 = manager.selectAll(table);
        Assert.assertTrue(result2.size() == 1);

        Bundle read2 = result2.get(0);
        Assert.assertEquals("New Message", read2.getString("message"));

        manager.deleteAll(table);
    }

}
