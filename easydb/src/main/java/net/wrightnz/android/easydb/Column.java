package net.wrightnz.android.easydb;

/**
 * Created on 16/06/17.
 *
 * @author Richard Wright
 */
public class Column {

    private final String name;
    private final String type;
    private final String constraints;

    public Column(String name, String type) {
        this(name, type, null);
    }

    public Column(String name, String type, String constraints) {
        this.name = name;
        this.type = type;
        this.constraints = constraints;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getConstraints() {
        return constraints;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getCreateStatement() {
        StringBuilder sb = new StringBuilder();
        sb.append(name);
        sb.append(" ");
        sb.append(type);
        if (constraints != null) {
            sb.append(" ");
            sb.append(constraints);
        }
        return sb.toString();
    }

}
