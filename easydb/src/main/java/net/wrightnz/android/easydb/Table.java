package net.wrightnz.android.easydb;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import java.util.ArrayList;
import java.util.List;

import static net.wrightnz.android.easydb.Logger.debug;

/**
 * Created on 19/06/17.
 *
 * @author Richard Wright
 *
 * Extends this class so map the fields of the Pojo to store in SQLite to the database columns
 * and types to use. Constraints may also be defined to enforce
 * cardinality  @see net.wrightnz.android.easydb.Column.
 *
 * @param <T> the type of the entity/Pojo that will be stored in the SQLite table.
 */
@SuppressWarnings({"WeakerAccess", "unused", "UnusedReturnValue"})
public abstract class Table<T> implements BaseColumns {

    private static final String TAG = Table.class.getSimpleName();

    protected static final Column COLUMN_ID = new Column(_ID, "INTEGER", "PRIMARY KEY");

    public String getTableName(){
        return getClass().getSimpleName().toLowerCase();
    }

    public abstract Column[] getColumns();

    /**
     *
     * @return String array of the column names of this table.
     */
    String[] getProjection() {
        String[] projection = new String[getColumns().length];
        for (int i = 0; i < getColumns().length; i++) {
            projection[i] = getColumns()[i].getName();
        }
        return projection;
    }

    /**
     *
     * @return the SQL create statement for this table
     */
    String getCreateStatement() {
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE ");
        sb.append(getTableName());
        sb.append(" ( ");
        for (int i = 0; i < getColumns().length; i++) {
            sb.append(getColumns()[i].getCreateStatement());
            if (i < getColumns().length - 1) {
                sb.append(", ");
            }
        }
        sb.append(" )");
        return sb.toString();
    }

    /**
     *
     * @return the SQL delete statement for this table
     */
    String getDeleteStatement() {
        return "DROP TABLE IF EXISTS " + getTableName();
    }

    public abstract ContentValues toContentValues(T data);

    public abstract T fromCursor(Cursor cursor);

    /**
     * Insert supplied data into this SQLite table
     * @param db   use getWritableDatabase()
     * @return the primary key of the newly inserted record.
     */
    public long insert(SQLiteDatabase db, T data) {
        debug(TAG,"Called: update(%s, %s)", db, data);
        ContentValues values = toContentValues(data);
        // Insert the new row, returning the primary key value of the new row
        return db.insert(getTableName(), null, values);
    }

    /**
     * Update a row in this SQLite table by primary key
     * @param db use getWritableDatabase()
     * @param data to Pojo to update
     * @param primaryKey the primary key of the row to update
     * @return number of rows updated 0 or 1
     */
    public int update(SQLiteDatabase db, T data, long primaryKey) {
        debug(TAG,"Called: update(%s, %s, %s)", db, data, primaryKey);
        // Create a new map of values, where column names are the keys
        ContentValues values = toContentValues(data);
        String[] selectionArgs = {String.valueOf(primaryKey)};
        return db.update(getTableName(), values, getColumns()[0].getName() + " = ?", selectionArgs);
    }


    public T select(SQLiteDatabase db, long primaryKey) {
        debug(TAG, "Called: select(%s, %s)", db, primaryKey);
        // Create a new map of values, where column names are the keys
        String[] selectionArgs = {String.valueOf(primaryKey)};
        List<T> results = select(db, Table._ID + " = ?", selectionArgs, null);
        if (results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }

    /**
     *
     * @param db use getReadableDatabase()
     * @return all rows in this this SQLite table.
     */
    public List<T> getAll(SQLiteDatabase db) {
        debug(TAG,"Called: getAll(%s)", db);
        List<T> results = new ArrayList<>();
        Cursor cursor = getCursor(db, null, null, null);
        while (cursor.moveToNext()) {
            results.add(fromCursor(cursor));
        }
        return results;
    }

    /**
     *
     * @param db use getReadableDatabase()
     * @return the first row in this this SQLite table.
     */
    public T getFirst(SQLiteDatabase db) {
        debug(TAG,"Called: getFirst(%s)", db);
        Cursor cursor = getCursor(db, null, null, null);
        return fromCursor(cursor);
    }

    /**
     * @param db use getReadableDatabase()
     * @param selection the selection clause. Use '?' to make parameters
     * @param selectionArgs the parameter values to place in the selection clause
     * @param sortOrder column name to sort by.
     * @return List of Pojos from this table that match this input selection criteria, or empty
     * List if no matches are found.
     */
    public List<T> select(SQLiteDatabase db, String selection, String[] selectionArgs, String sortOrder) {
        debug(TAG,"Called: select(%s, %s)", db, selection);
        List<T> results = new ArrayList<>();
        Cursor cursor = getCursor(db, selection, selectionArgs, sortOrder);
        while (cursor.moveToNext()) {
            results.add(fromCursor(cursor));
        }
        return results;
    }

    /**
     * Delete all rows from this SQLite table
     *
     * @param db use getWritableDatabase()
     */
    public int deleteAll(SQLiteDatabase db) {
        debug(TAG,"Called: deleteAll(%s)", db);
        return db.delete(getTableName(), null, null);
    }

    private Cursor getCursor(SQLiteDatabase db, String selection, String[] selectionArgs, String sortOrder) {
        debug(TAG,"Called: select(%s, %s)", db, selection);
        return db.query(
                getTableName(),      // The table to query
                getProjection(),     // The columns to return
                selection,           // The columns for the WHERE clause
                selectionArgs,       // The values for the WHERE clause
                null,        // don't group the rows
                null,         // don't filter by row groups
                sortOrder            // The sort order
        );
    }


}
