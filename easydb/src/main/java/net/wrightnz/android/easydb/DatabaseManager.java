package net.wrightnz.android.easydb;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

/**
 * Created on 19/12/17.
 *
 * @author Richard Wright
 */
@SuppressWarnings("TryFinallyCanBeTryWithResources")
public class DatabaseManager {

    private static DatabaseManager instance;

    private DbHelper helper;

    public static DatabaseManager getInstance(Context context, DatabaseDefinition definition){
        if(instance == null) {
            instance = new DatabaseManager(context, definition);
        }
        return instance;
    }

    private DatabaseManager(Context context, DatabaseDefinition definition){
        helper = new DbHelper(context, definition);
    }

    public <T> long insert(Table<T> table, T data) {
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            return table.insert(db, data);
        } finally {
            db.close();
        }
    }

    public <T> T select(Table<T> table, long primaryKey) {
        SQLiteDatabase db = helper.getReadableDatabase();
        try {
            return table.select(db, primaryKey);
        } finally {
            db.close();
        }
    }

    public <T> int update(Table<T> table, T data, long primaryKey) {
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            return table.update(db, data, primaryKey);
        } finally {
            db.close();
        }
    }

    public void deleteAll(Table table) {
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            table.deleteAll(db);
        } finally {
            db.close();
        }
    }

    public <T> List<T> selectAll(Table<T> table) {
        SQLiteDatabase db = helper.getReadableDatabase();
        try {
            return table.getAll(db);
        } finally {
            db.close();
        }
    }

}
