package net.wrightnz.android.easydb;

import java.util.List;

/**
 * Created for (Spark NZ) on 19/12/17.
 *
 * @author Richard Wright
 */

public interface DatabaseDefinition {

    String getName();

    int getVersion();

    List<? extends Table> getTables();

}
