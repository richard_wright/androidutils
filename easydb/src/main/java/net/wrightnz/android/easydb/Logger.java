package net.wrightnz.android.easydb;

import android.util.Log;

/**
 * Created for (Spark NZ) on 20/12/17.
 *
 * @author Richard Wright
 */

public class Logger {

    private static final boolean DEBUG = true;

    public static void debug(String tag, String message, Object... args) {
        if (DEBUG) {
            Log.d(tag, String.format(message, args));
        }
    }
}
