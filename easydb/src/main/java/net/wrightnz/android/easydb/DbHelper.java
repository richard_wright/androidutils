package net.wrightnz.android.easydb;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created on 19/12/17.
 *
 * @author Richard Wright
 */
public class DbHelper extends SQLiteOpenHelper {

    private final DatabaseDefinition definition;

    public DbHelper(Context context, DatabaseDefinition definition) {
        super(context, definition.getName(), null, definition.getVersion());
        this.definition = definition;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (Table table : definition.getTables()) {
            db.execSQL(table.getCreateStatement());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (Table table : definition.getTables()) {
            db.execSQL(table.getDeleteStatement());
        }
        onCreate(db);
    }

    public DatabaseDefinition getDefinition() {
        return definition;
    }
}
